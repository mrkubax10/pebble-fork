meta:
  id: gamedata
  endian: le
seq:
  - id: count
    type: u4
  - id: paths
    type: file
    repeat: expr
    repeat-expr: count
  - id: contents
    size: paths[_index].size
    repeat: expr
    repeat-expr: count
types:
  file:
    seq:
      - id: size
        type: u4
      - id: len
        type: u4
      - id: offset
        type: u4
      - id: name
        type: str
        size: len
        encoding: ASCII