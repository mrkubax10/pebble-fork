from gamedata import *
from pathlib import Path

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("gamedata_path")
args = parser.parse_args()

gamedata_parsed = Gamedata.from_file(args.gamedata_path)

print("Found", gamedata_parsed.count, "entries")
print("Unpacking...")

Path("ASSETS").mkdir(exist_ok=True)

for i in range(len(gamedata_parsed.paths)):
	required_directory = gamedata_parsed.paths[i].name.split('/')[:-1]
	file_name = gamedata_parsed.paths[i].name.split('/')[-1]

	full_curr_path = ""
	for directory in required_directory:
		Path(full_curr_path + directory).mkdir(exist_ok=True)
		full_curr_path += directory + '/'

	asset_file = open(gamedata_parsed.paths[i].name, "wb")
	asset_file.write(gamedata_parsed.contents[i])
	asset_file.close()
