/// Small tool to see what some functions return
///
/// Outputs:
/// - get_data_path
/// - _get_data_path
///

#include <stdio.h>

#include "common/data.h"

#define LOGF(msg, ...)                          \
	do {                                        \
		fprintf(stderr, msg "\n", __VA_ARGS__); \
	} while (0)

int
main()
{
	LOGF("get_data_path(): %s", get_data_path());
	LOGF("_get_data_path(): %s", _get_data_path());
	return 0;
}
