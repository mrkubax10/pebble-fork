# pebble

This is a work-in-progress reimplementation of the original game "Perypetie Boba" featuring more concise code, improved stability, and hopefully some new features. The project is still under development.

## Building

```
meson build
ninja -C build
```

## Tooling and commands

### Using address sanitizer

Just set your compiler to clang and add `-Db_sanitize=address,undefined`.

**WARNING:** You can use gcc, but expect much worse results when it comes to details, so prefer clang if possible.

```
CC=clang meson build -Db_sanitize=address,undefined
```

### Clang-format

If possible, we recommend using an editor with the ability to format files after saving, like Visual Studio Code.

In a modern shell:

```
clang-format src/**.c src/**.h engine/**.c engine/**.h -i
```

### Other useful things

Remove audio: `meson build -Daudio_backend=dummy`

Use clang-tidy: `ninja -C build clang-tidy`
