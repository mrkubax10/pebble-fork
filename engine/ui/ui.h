/// @addtogroup ui User interface
/// @{
/// This is pebble's current user interface library. It is written with our engine in mind,
/// and our weird requirements and usage. It uses the imgui pattern for flexibility and
/// simplicity.
///
/// There are four concepts around this:
/// - Context
/// - Layout
/// - Widget
/// - Command
///
/// **Context** is the most important here. It manages how widgets will work, look,
/// will they be rendered and so on. It contains pointers to important things, like screen,
/// font, input manager, current layout or queue.
///
/// **Layouts** manages the widgets' size and position. They are pretty simple, but still
/// require some work, like ui_layout_add_child_fn callback, so layouts can extend themselves
/// and update position where next widget will be.
///
/// **Widgets** are the things that interact with the user somehow. Labels, buttons, checkboxes,
/// et cetera.
///
/// **Command** is what the context uses to draw widgets, and what layouts and widgets use to
/// represent themselves. The context needs to know how to render layouts and widgets, so this
/// is what commands are for. They tell the context what it should do - where to render the
/// widget, what to render, and what to do after rendering it.
///
/// For now, we have two commands:
/// - DRAW_SURFACE
/// - DRAW_RECT.
///
/// See also ui_cmd_draw_surf() and ui_cmd_draw_rect().
///
/// You can find commonly used widgets here:
/// ui/checkbox.h,
/// ui/image.h,
/// ui/label.h
///
/// You can find common layouts here:
/// ui/menu.h
///
/// Example using ui:
/// @code{.c}
/// // in init function:
/// struct surface *screen = ...;
/// struct font *font = ...;
/// struct input *input = ...;
/// struct ui_style style = ...;
/// struct ui *ctx = ui_create(screen, font, &input, &style);
/// 
/// // in update function:
/// ui_begin(ctx);
///
/// ui_fixed_begin(ctx, vec2i(2, 2), 3);
/// {
/// 	ui_label(ctx, "Hello!");
/// }
/// ui_fixed_end(ctx);
///
/// // in render function:
/// ui_render(ctx);
/// @endcode

#ifndef UI_H
#define UI_H

#include <stdbool.h>

#include "backends/render/input_base.h"
#include "common/graphics/color.h"
#include "common/graphics/surface.h"
#include "common/graphics/text.h"
#include "common/math/vector.h"

struct ui_context;

typedef void (*ui_layout_add_child_fn)(struct ui_context *, vec2i_t);

#define UI_LAYOUT_HEADER \
	struct recti rect;   \
	vec2i_t next;   \
	int queue_pos;       \
	ui_layout_add_child_fn add_child_impl;

struct ui_layout {
	UI_LAYOUT_HEADER
};

struct ui_command_surf {
	struct surface *surf;
	vec2i_t pos;
	struct recti *rect;
	bool clean;
};

struct ui_command_rect {
	struct recti rect;
	struct color_rgba color;
};

struct ui_command {
	enum ui_command_kind { DRAW_SURFACE, DRAW_RECT } kind;

	union _uicmd {
		struct ui_command_surf surf;
		struct ui_command_rect rect;
	} c;
};

struct ui_style {
	struct surface *style;
	struct recti checkbox_checked;
	struct recti checkbox;
};

#define COMMAND_QUEUE_SIZE 256
struct ui_context {
	struct surface *screen;
	struct ui_style *style;
	struct font *font;
	struct ui_layout *layout;
	struct input *input;

	// private
	struct ui_command command_queue[COMMAND_QUEUE_SIZE];
	int queue_len;
	bool locked;
};

/// @brief Creates a new ui context.
/// @param screen Pointer to surface, will be used for rendering. Must not be NULL.
/// @param font Pointer to font, will be used for widgets that require text. Must not be NULL.
/// @return Valid pointer to ui_context
/// @note Panics if memory allocation fails
/// @see ui_begin
struct ui_context *ui_create(struct surface *screen, struct font *font, struct input *input,
                             struct ui_style *style);

/// @brief Begins drawing ui.
///
/// This function **must be called**, before drawing ui. If it isn't used, ctx will be
/// **locked**, and no rendering will happen. However, widgets can still interact with the user,
/// handle input, allocate surfaces etc. because that isn't done by ui,
/// but by the widgets themselves. This function also manages
/// the widgets' memory, so without it, memory leaks will happen.
///
/// @param ctx Pointer to ui_context. Must not be NULL.
void ui_begin(struct ui_context *ctx);

/// @brief Informs the current layout that a new widget is about to be rendered.
///
/// This function should be used only by widgets. However, it is still possible to use it,
/// if necessary.
/// When a new widget is created, the layout needs to calculate its size. This is why this
/// function exists.
///
/// @param ctx Pointer to ui_context. Must not be NULL.
/// @param size Widget's size.
void ui_layout_add_child(struct ui_context *ctx, vec2i_t size);

struct ui_command ui_cmd_draw_surf(struct surface *surf, vec2i_t pos, struct recti *rect,
                                   bool clean);
struct ui_command ui_cmd_draw_rect(struct recti rect, struct color_rgba color);

/// @brief Reserves space for a command.
///
/// This is only useful for layouts, as they often need to calculate their size.
/// For example, ui menu. Thanks to this function and ui_set_cmd(), it is possible to place
/// command when the widget is ready.
///
/// @param ctx Pointer to struct ui_context. Must not be NULL.
/// @return Value greater than 0 on success, negative code if ui is locked or queue is full.
/// @see ui_set_cmd
int ui_reserve_cmd(struct ui_context *ctx);

/// @brief Places a command at the given position.
/// @param ctx Pointer to ui context. Must not be NULL.
/// @param cmd Command that will be placed in queue.
/// @param pos Position in queue. -1 is valid value, and it does nothing. This is used so when
/// queue is full, widgets just don't appear.
/// @see ui_reserve_cmd
void ui_set_cmd(struct ui_context *ctx, struct ui_command cmd, int pos);

/// @brief Pushes a command to the command queue.
///
/// This is what's used for implementing widgets, and what you're looking for when
/// you are creating your own widget. It pushes a command to the command queue
/// using ui_reserve_cmd() and ui_set_cmd().
///
/// @param ctx Pointer to ui context. Must not be NULL.
/// @param cmd Command that will be placed in queue.
void ui_push_cmd(struct ui_context *ctx, struct ui_command cmd);
void ui_render(struct ui_context *ctx);

#endif

/// @}
