#ifndef UI_CHECKBOX_H
#define UI_CHECKBOX_H

#include <stdbool.h>

#include "ui.h"

bool ui_checkbox(struct ui_context *ctx, const char *text, bool checked);

#endif
