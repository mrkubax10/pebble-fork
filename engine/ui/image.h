#ifndef UI_IMAGE_H
#define UI_IMAGE_H

#include "common/graphics/surface.h"
#include "ui.h"

void ui_image_clip(struct ui_context *ctx, struct surface *surf, struct recti *rect);
void ui_image(struct ui_context *ctx, struct surface *surf);

#endif
