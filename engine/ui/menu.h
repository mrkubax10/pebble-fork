#ifndef UI_MENU_H
#define UI_MENU_H

#include "common/math/vector.h"
#include "ui.h"

struct ui_menu {
	UI_LAYOUT_HEADER
	int child_padding;
	int count;
};

void ui_menu_begin(struct ui_context *ctx, vec2i_t pos, int child_padding);
void ui_menu_end(struct ui_context *ctx);

#endif
