#include "backends/threads/mutex.h"

#include <pthread.h>
#include <stdlib.h>

#include "common/util.h"

struct mutex {
	pthread_mutex_t native_mutex;
};

struct mutex *
mutex_create()
{
	struct mutex *mutex = emalloc(sizeof(struct mutex));
	pthread_mutex_init(&mutex->native_mutex, NULL);
	return mutex;
}

void
mutex_free(struct mutex **mutex)
{
	if (*mutex) {
		pthread_mutex_destroy(&(*mutex)->native_mutex);
		free(*mutex);
		*mutex = NULL;
	}
}

void
mutex_lock(struct mutex *mutex)
{
	pthread_mutex_lock(&mutex->native_mutex);
}

void
mutex_unlock(struct mutex *mutex)
{
	pthread_mutex_unlock(&mutex->native_mutex);
}
