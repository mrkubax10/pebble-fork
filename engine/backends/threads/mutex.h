#ifndef ENGINE_MUTEX_H
#define ENGINE_MUTEX_H

/// @brief Creates and initializes a new mutex.
///
/// @returns the newly created mutex
struct mutex *mutex_create();

/// @brief Frees the mutex.
///
/// @param mutex the mutex to release
void mutex_free(struct mutex **mutex);

/// @brief Locks the mutex.
///
/// If another thread attempts to lock the mutex, it will block until the mutex
/// is unlocked.
///
/// @param the mutex to lock
void mutex_lock(struct mutex *mutex);

/// @brief Unlocks the mutex.
///
/// Lets another thread lock the mutex.
///
/// @param the mutex to unlock
void mutex_unlock(struct mutex *mutex);

#endif
