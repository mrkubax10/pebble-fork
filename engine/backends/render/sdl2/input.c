#include "backends/render/input.h"

#include <SDL.h>
#include <string.h>

#include "SDL_gamecontroller.h"

void
input_init(struct input *input)
{
	memset(input, 0, sizeof(*input));
}

static inline enum key
map_key(SDL_Scancode sc)
{
	switch (sc) {
	case SDL_SCANCODE_UP:
		return KEY_UP;
	case SDL_SCANCODE_DOWN:
		return KEY_DOWN;
	case SDL_SCANCODE_LEFT:
		return KEY_LEFT;
	case SDL_SCANCODE_RIGHT:
		return KEY_RIGHT;
	case SDL_SCANCODE_Z:
		return KEY_JUMP;
	case SDL_SCANCODE_X:
		return KEY_SHOOT;
	case SDL_SCANCODE_ESCAPE:
		return KEY_ESC;
	case SDL_SCANCODE_RETURN:
		return KEY_ENTER;
	case SDL_SCANCODE_F2:
		return KEY_CHEAT_DEBUG_MENU;
	case SDL_SCANCODE_1:
		return KEY_CHEAT_FLYHACK;
	case SDL_SCANCODE_2:
		return KEY_CHEAT_GODMODE;
	case SDL_SCANCODE_3:
		return KEY_CHEAT_PHYSICS_VERBOSE_ENTITIES;
	case SDL_SCANCODE_4:
		return KEY_CHEAT_PHYSICS_VERBOSE_TILES;
	case SDL_SCANCODE_5:
		return KEY_CHEAT_DISABLE_SHADOWS;
	case SDL_SCANCODE_KP_4:
		return KEY_CHEAT_LOCATION_LEFT;
	case SDL_SCANCODE_KP_6:
		return KEY_CHEAT_LOCATION_RIGHT;
	case SDL_SCANCODE_KP_8:
		return KEY_CHEAT_LOCATION_UP;
	case SDL_SCANCODE_KP_2:
		return KEY_CHEAT_LOCATION_DOWN;
	case SDL_SCANCODE_F3:
		return KEY_STATS;
	default:
		return KEY_NONE;
	}
}

static inline void
mark_just_released(press_state_t *p)
{
	*p |= STATE_JUST_RELEASED;
	*p &= ~STATE_IS_DOWN;
}

void
on_controller_button_up(struct input *input, SDL_ControllerButtonEvent e)
{
	switch (e.button) {
	case SDL_CONTROLLER_BUTTON_DPAD_UP:
		mark_just_released(&input->key_states[input->joystick_binds.dpad_up]);
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		mark_just_released(&input->key_states[input->joystick_binds.dpad_down]);
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		mark_just_released(&input->key_states[input->joystick_binds.dpad_left]);
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		mark_just_released(&input->key_states[input->joystick_binds.dpad_right]);
		break;

	case SDL_CONTROLLER_BUTTON_START:
		mark_just_released(&input->key_states[input->joystick_binds.start]);
		break;
	case SDL_CONTROLLER_BUTTON_GUIDE:
		mark_just_released(&input->key_states[input->joystick_binds.select]);
		break;

	case SDL_CONTROLLER_BUTTON_A:
		mark_just_released(&input->key_states[input->joystick_binds.X]);
		break;
	case SDL_CONTROLLER_BUTTON_B:
		mark_just_released(&input->key_states[input->joystick_binds.O]);
		break;
	case SDL_CONTROLLER_BUTTON_Y:
		mark_just_released(&input->key_states[input->joystick_binds.TR]);
		break;
	case SDL_CONTROLLER_BUTTON_X:
		mark_just_released(&input->key_states[input->joystick_binds.SQ]);
		break;

	case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
		mark_just_released(&input->key_states[input->joystick_binds.LB]);
		break;
	case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
		mark_just_released(&input->key_states[input->joystick_binds.RB]);
		break;
	}
}

void
on_controller_button_down(struct input *input, SDL_ControllerButtonEvent e)
{
	switch (e.button) {
	case SDL_CONTROLLER_BUTTON_DPAD_UP:
		input->key_states[input->joystick_binds.dpad_up] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		input->key_states[input->joystick_binds.dpad_down] |=
		    STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		input->key_states[input->joystick_binds.dpad_left] |=
		    STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		input->key_states[input->joystick_binds.dpad_right] |=
		    STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;

	case SDL_CONTROLLER_BUTTON_START:
		input->key_states[input->joystick_binds.start] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_GUIDE:
		input->key_states[input->joystick_binds.select] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;

	case SDL_CONTROLLER_BUTTON_A:
		input->key_states[input->joystick_binds.X] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_B:
		input->key_states[input->joystick_binds.O] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_Y:
		input->key_states[input->joystick_binds.TR] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_X:
		input->key_states[input->joystick_binds.SQ] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;

	case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
		input->key_states[input->joystick_binds.LB] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
		input->key_states[input->joystick_binds.RB] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
		break;
	}
}

void
input_poll_events(struct input *input, struct window *window)
{
	SDL_Event e;

	while (SDL_PollEvent(&e)) {

		switch (e.type) {

		case SDL_QUIT:
			if (input->quit_callback != NULL)
				input->quit_callback();
			break;

		case SDL_MOUSEMOTION: {
			vec2i_t mouse;
			struct recti display_boundary = window_get_display_boundary(window);
			vec2i_t streaming_texture_res = window_get_streaming_texture_res(window);

			mouse.x = ((e.motion.x - display_boundary.x) / (float)display_boundary.w) *
			          streaming_texture_res.x;
			mouse.y = ((e.motion.y - display_boundary.y) / (float)display_boundary.h) *
			          streaming_texture_res.y;

			input->mouse = mouse;
			break;
		}

		case SDL_KEYDOWN:
			if (e.key.repeat)
				break;
			input->key_states[map_key(e.key.keysym.scancode)] |=
			    STATE_JUST_PRESSED | STATE_IS_DOWN;
			break;

		case SDL_KEYUP:
			input->key_states[map_key(e.key.keysym.scancode)] |= STATE_JUST_RELEASED;
			input->key_states[map_key(e.key.keysym.scancode)] &= ~STATE_IS_DOWN;
			break;

		case SDL_MOUSEBUTTONDOWN:
			input->mouse_button_states[e.button.button] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
			break;

		case SDL_MOUSEBUTTONUP:
			input->mouse_button_states[e.button.button] |= STATE_JUST_RELEASED;
			input->mouse_button_states[e.button.button] &= ~STATE_IS_DOWN;
			break;

		case SDL_MOUSEWHEEL:
			input->wheel = vec2i(e.wheel.x, e.wheel.y);
			break;

		case SDL_CONTROLLERBUTTONUP:
			on_controller_button_up(input, e.cbutton);
			break;
		case SDL_CONTROLLERBUTTONDOWN:
			on_controller_button_down(input, e.cbutton);
			break;
		}
	}
}

void
input_tick(struct input *input)
{
	for (int i = 0; i < KEY__LAST; ++i) {
		input->key_states[i] &= ~(STATE_JUST_PRESSED | STATE_JUST_RELEASED);
	}
	for (int i = 0; i < MOUSE_BUTTON__LAST; ++i) {
		input->mouse_button_states[i] &= ~(STATE_JUST_PRESSED | STATE_JUST_RELEASED);
	}
	input->wheel = vec2i(0, 0);
}

extern inline bool input_key(struct input *input, enum key key, press_state_t mask);

extern inline bool input_mouse_button(struct input *input, enum mouse_button mb,
                                      press_state_t mask);

extern inline void input_on_quit(struct input *input, input_quit_fn callback);
