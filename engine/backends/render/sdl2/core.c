#include "backends/render/core.h"

#include <SDL.h>

void
core_init()
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER);
}

void
core_quit()
{
	SDL_Quit();
}

int
display_get_resolution(int *w, int *h)
{
	SDL_DisplayMode dm;
	int status = 0;

	if ((status = SDL_GetDesktopDisplayMode(0, &dm)) < 0) {
		return status;
	}

	*w = dm.w;
	*h = dm.h;

	return 0;
}
