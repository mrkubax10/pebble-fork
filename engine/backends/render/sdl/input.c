#include "backends/render/input.h"

#include <SDL.h>
#include <string.h>

#include "SDL_events.h"

void
input_init(struct input *input)
{
	memset(input, 0, sizeof(*input));
}

static inline enum key
map_key(SDLKey key)
{
	switch (key) {
	case SDLK_UP:
		return KEY_UP;
	case SDLK_DOWN:
		return KEY_DOWN;
	case SDLK_LEFT:
		return KEY_LEFT;
	case SDLK_RIGHT:
		return KEY_RIGHT;
	case SDLK_z:
		return KEY_JUMP;
	case SDLK_x:
		return KEY_SHOOT;
	case SDLK_ESCAPE:
		return KEY_ESC;
	case SDLK_RETURN:
		return KEY_ENTER;
	case SDLK_F2:
		return KEY_CHEAT_DEBUG_MENU;
	case SDLK_1:
		return KEY_CHEAT_FLYHACK;
	case SDLK_2:
		return KEY_CHEAT_GODMODE;
	case SDLK_KP4:
		return KEY_CHEAT_LOCATION_LEFT;
	case SDLK_KP6:
		return KEY_CHEAT_LOCATION_RIGHT;
	case SDLK_KP8:
		return KEY_CHEAT_LOCATION_UP;
	case SDLK_KP2:
		return KEY_CHEAT_LOCATION_DOWN;
	default:
		return KEY_NONE;
	}
}

static inline void
mark_just_released(press_state_t *p)
{
	*p |= STATE_JUST_RELEASED;
	*p &= ~STATE_IS_DOWN;
}

void
input_poll_events(struct input *input, struct window *window)
{
	(void)window;

// NOTE(legalprisoner8140):
// I can't find anything in the documentation on how mousewheel works in SDL 1.2.
// I guess we'll just ignore it for now.
#define IGNORE_MOUSEWHEEL                                                                 \
	if (e.button.button == SDL_BUTTON_WHEELUP || e.button.button == SDL_BUTTON_WHEELDOWN) \
	break

// NOTE(legalprisoner8140):
// Because I'm lazy I will just remap mouse button
#define MAP_BUTTON                                 \
	Uint8 button = e.button.button;                \
	if (e.button.button == SDL_BUTTON_X1) {        \
		button = MOUSE_BUTTON_X1;                  \
	} else if (e.button.button == SDL_BUTTON_X2) { \
		button = MOUSE_BUTTON_X2;                  \
	}

	SDL_Event e;

	while (SDL_PollEvent(&e)) {
		switch (e.type) {

		case SDL_QUIT:
			if (input->quit_callback != NULL)
				input->quit_callback();
			break;

		case SDL_MOUSEMOTION: {
			vec2i_t mouse;
			struct recti display_boundary = window_get_display_boundary(window);
			vec2i_t streaming_texture_res = window_get_streaming_texture_res(window);

			mouse.x = ((e.motion.x - display_boundary.x) / (float)display_boundary.w) *
			          streaming_texture_res.x;
			mouse.y = ((e.motion.y - display_boundary.y) / (float)display_boundary.h) *
			          streaming_texture_res.y;

			input->mouse = mouse;
			break;
		}

		case SDL_KEYDOWN:
			input->key_states[map_key(e.key.keysym.sym)] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
			break;

		case SDL_KEYUP:
			input->key_states[map_key(e.key.keysym.sym)] |= STATE_JUST_RELEASED;
			input->key_states[map_key(e.key.keysym.sym)] &= ~STATE_IS_DOWN;
			break;

		case SDL_MOUSEBUTTONDOWN: {
			IGNORE_MOUSEWHEEL;
			MAP_BUTTON;

			input->mouse_button_states[button] |= STATE_JUST_PRESSED | STATE_IS_DOWN;
			break;
		}

		case SDL_MOUSEBUTTONUP: {
			IGNORE_MOUSEWHEEL;
			MAP_BUTTON;

			input->mouse_button_states[button] |= STATE_JUST_RELEASED;
			input->mouse_button_states[button] &= ~STATE_IS_DOWN;
			break;
		}
		}

		// NOTE(Firstbober)
		// This is implementation of PSP controller layout.
		// It will be commented out for now, until proper multi platform support will be added
		/*
		if (e.type == SDL_JOYBUTTONUP) {
		    printf("%i\n", e.jbutton.button);
		    switch (e.jbutton.button) {
		    // D-Pad

		    // Up
		    case 8:
		        mark_just_released(&input->key_states[input->joystick_binds.dpad_up]);
		        break;
		    // Down
		    case 6:
		        mark_just_released(&input->key_states[input->joystick_binds.dpad_down]);
		        break;
		    // Left
		    case 7:
		        mark_just_released(&input->key_states[input->joystick_binds.dpad_left]);
		        break;
		    // Right
		    case 9:
		        mark_just_released(&input->key_states[input->joystick_binds.dpad_right]);
		        break;

		    // Start & Select
		    case 11:
		        mark_just_released(&input->key_states[input->joystick_binds.start]);
		        break;
		    case 10:
		        mark_just_released(&input->key_states[input->joystick_binds.select]);
		        break;

		    // Actions

		    // Cross
		    case 2:
		        mark_just_released(&input->key_states[input->joystick_binds.X]);
		        break;
		    // Circle
		    case 1:
		        mark_just_released(&input->key_states[input->joystick_binds.O]);
		        break;
		    // Triangle
		    case 0:
		        mark_just_released(&input->key_states[input->joystick_binds.TR]);
		        break;
		    // Square
		    case 3:
		        mark_just_released(&input->key_states[input->joystick_binds.SQ]);
		        break;

		    // LB & RB
		    case 4:
		        mark_just_released(&input->key_states[input->joystick_binds.LB]);
		        break;
		    case 5:
		        mark_just_released(&input->key_states[input->joystick_binds.RB]);
		        break;

		    default:
		        break;
		    }
		} else if (e.type == SDL_JOYBUTTONDOWN) {
		    switch (e.jbutton.button) {
		    // D-Pad

		    // Up
		    case 8:
		        input->key_states[input->joystick_binds.dpad_up] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    // Down
		    case 6:
		        input->key_states[input->joystick_binds.dpad_down] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    // Left
		    case 7:
		        input->key_states[input->joystick_binds.dpad_left] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    // Right
		    case 9:
		        input->key_states[input->joystick_binds.dpad_right] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;

		    // Actions

		    // Cross
		    case 2:
		        input->key_states[input->joystick_binds.X] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    // Circle
		    case 1:
		        input->key_states[input->joystick_binds.O] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    // Triangle
		    case 0:
		        input->key_states[input->joystick_binds.TR] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    // Square
		    case 3:
		        input->key_states[input->joystick_binds.SQ] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;

		    // LB & RB
		    case 4:
		        input->key_states[input->joystick_binds.LB] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;
		    case 5:
		        input->key_states[input->joystick_binds.RB] |=
		            STATE_JUST_PRESSED | STATE_IS_DOWN;
		        break;

		    default:
		        break;
		    }
		}
		*/
	}
}

void
input_tick(struct input *input)
{
	for (int i = 0; i < KEY__LAST; ++i) {
		input->key_states[i] &= ~(STATE_JUST_PRESSED | STATE_JUST_RELEASED);
	}
	for (int i = 0; i < MOUSE_BUTTON__LAST; ++i) {
		input->mouse_button_states[i] &= ~(STATE_JUST_PRESSED | STATE_JUST_RELEASED);
	}
	input->wheel = vec2i(0, 0);
}

extern inline bool input_key(struct input *input, enum key key, press_state_t mask);

extern inline bool input_mouse_button(struct input *input, enum mouse_button mb,
                                      press_state_t mask);

extern inline void input_on_quit(struct input *input, input_quit_fn callback);
