#include "backends/render/core.h"

#include <SDL.h>
#include <assert.h>
#include <stdio.h>

#include "common/log.h"

void
core_init()
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);
}

void
core_quit()
{
	SDL_Quit();
}

int
display_get_resolution(int *w, int *h)
{
	assert(w != NULL);
	assert(h != NULL);

	const SDL_VideoInfo *info = SDL_GetVideoInfo();

	*w = info->current_w;
	*h = info->current_h;

	return 0;
}
