#include "backends/audio/audio.h"

struct chunk_play_params
chunk_play_params_defaults()
{
	struct chunk_play_params params;
	params.chunk = NULL;
	params.volume = 255;
	return params;
}
