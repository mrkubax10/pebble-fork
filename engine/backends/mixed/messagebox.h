/// @addtogroup useful
/// @{

#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <stdint.h>
#include <stdio.h>

#include "common/util.h"

/// @brief Displays a message box.
/// @see messageboxf
void messagebox(const char *title, const char *message);

/// @brief Displays a message box with formatted message.
/// @param message The format string.
#define messageboxf(title, message, ...)              \
	do {                                              \
		char *_buf = dsprintf(message, __VA_ARGS__);  \
		LOG_INFOF("messagebox: %s, %s", title, _buf); \
		messagebox(title, _buf);                      \
		free(_buf);                                   \
	} while (0)

#endif

/// @}
