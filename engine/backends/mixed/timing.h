#ifndef TIME_H
#define TIME_H

#include <stdint.h>

uint64_t get_ticks();

void time_init();

/// @brief Returns time (measured from start of the application) in milliseconds
uint64_t time_get_millis();

/// @brief Returns time (measured from start of the application) in seconds
double time_get_seconds();

#endif
