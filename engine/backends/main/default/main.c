
#include <stdio.h>

#include "backends/render/render.h"

#ifdef _RENDER_BACKEND_sdl2
#include <SDL.h>
#endif

extern int t_main(int argc, char **argv);

int
main(int argc, char *argv[])
{
	core_init();
	int ret = t_main(argc, argv);
	core_quit();
	return ret;
}
