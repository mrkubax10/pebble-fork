#ifndef ENGINE_SYS_FS
#define ENGINE_SYS_FS

#include <stdbool.h>

enum { FS_OK = 0, FS_ALREADY_EXISTS = -1, FS_PATH_INVALID = -2, FS_UNKNOWN = -3 };

enum fs_file_type { FS_IS_FILE, FS_IS_DIRECTORY, FS_IS_UNKNOWN };

struct fs_dir;

struct fs_file {
	char *name;
	enum fs_file_type type;
};

/// @brief Creates a new directory.
///
/// The behavior of this function is not explicit.
/// It may vary depending on the function used in the code or platform.
/// For example, on win32 `create_dir` is limited to 248 characters,
/// due to the use of CreateDirectoryA. By the way, some, or even all,
/// of the errors may be ignored. (Less likely if win32 is used)
///
/// @param path The path of the directory to be created.
/// @return FS_OK on success, negative on error. See FS_*.
int fs_create_dir(const char *path);

/// @brief Check if path is valid.
/// @param path The path to be checked
/// @return true if exists, otherwise false
bool fs_is_valid_path(const char *path);

/// @brief Checks if a file is a directory.
/// @param path The path to a file
bool fs_is_directory(const char *path);

/// @brief Checks if a file is a regular file.
/// @param path The path to a file
bool fs_is_file(const char *path);

/// @brief Begins walking a directory.
/// @param path The path to be directory that will be walked, must not be NULL.
/// @return NULL on error
struct fs_dir *fs_walk_start(const char *path);

/// @brief Walks a directory.
///
/// Example:
/// @code{.c}
/// struct fs_file *file;
/// struct fs_dir *dir = fs_walk_start("/path/to/directory");
///
/// while ((file = fs_walk_dir(dir)) != NULL) {
/// 	// ...
/// }
///
/// fs_walk_end(dir);
/// @endcode
///
/// @param dir Iterator
/// @return NULL if there are no more files or if function failed
struct fs_file *fs_walk_dir(struct fs_dir *dir);

/// @brief Ends walking a directory.
/// @param dir Iterator
void fs_walk_end(struct fs_dir *dir);

char *fs_path_filename(const char *path);

#endif
