#include "util.h"

extern inline int clampi(int x, int min, int max);
extern inline float clampf(float x, float min, float max);

extern inline float lerpf(float v0, float v1, float t);
