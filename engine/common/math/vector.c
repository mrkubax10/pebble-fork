#include "vector.h"

extern inline vec2_t vec2(float x, float y);
extern inline vec2i_t vec2i(int x, int y);

extern inline vec2_t to_vec2(vec2i_t v);
extern inline vec2i_t to_vec2i(vec2_t v);

#define VECTOR_OP_VV(funcname) extern inline vec2_t funcname(vec2_t u, vec2_t v);

VECTOR_OP_VV(add2v)
VECTOR_OP_VV(sub2v)
VECTOR_OP_VV(mul2v)
VECTOR_OP_VV(div2v)

#undef VECTOR_OP_VV

#define VECTOR_OP_VF(funcname) extern inline vec2_t funcname(vec2_t u, float x);

VECTOR_OP_VF(add2f)
VECTOR_OP_VF(sub2f)
VECTOR_OP_VF(mul2f)
VECTOR_OP_VF(div2f)

#undef VECTOR_OP_VF
