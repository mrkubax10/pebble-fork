#include "rect.h"

extern inline struct rectf rectf(float, float, float, float);
extern inline struct recti recti(int, int, int, int);
extern inline struct rectf rectf_sides(float, float, float, float);
extern inline struct recti recti_sides(int, int, int, int);

#define RECT_SIDES(T, DT, suffix)              \
	extern inline DT rleft##suffix(struct T);  \
	extern inline DT rright##suffix(struct T); \
	extern inline DT rtop##suffix(struct T);   \
	extern inline DT rbottom##suffix(struct T);

RECT_SIDES(rectf, float, f)
RECT_SIDES(recti, int, i)

#undef RECT_SIDES

extern inline bool rintersect(struct rectf *, struct rectf *);

extern inline bool rcontains(struct rectf *, vec2_t);
