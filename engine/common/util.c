#include "util.h"

#include <stdlib.h>

#include "backends/render/render.h"

#ifndef UTIL_NO_RENDER
int
adjust_size(int *w, int *h)
{
	int status = 0;
	int width, height;

	if ((status = display_get_resolution(&width, &height)) < 0) {
		return status;
	}

	width /= 320;
	height /= 180;

	int ratio = width < height ? width : height;

	if (ratio - 2 > 0)
		ratio -= 2;
	else
		ratio = 1;

	if (w)
		*w *= ratio;
	if (h)
		*h *= ratio;

	return 0;
}
#endif

void
oom_panic(void)
{
	fputs("Out of memory\n", stderr);
}

void *
emalloc(size_t size)
{
	void *p = malloc(size);
	if (!p) {
		oom_panic();
	}
	return p;
}

void *
ecalloc(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);
	if (!p) {
		oom_panic();
	}
	return p;
}

void *
erealloc(void *ptr, size_t size)
{
	void *p = realloc(ptr, size);
	if (!p) {
		oom_panic();
	}
	return p;
}
