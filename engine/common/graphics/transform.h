/// @addtogroup graphics
/// @{

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "common/graphics/color.h"
#include "common/graphics/surface.h"
#include "common/math/vector.h"

#define TRANSFORM_STACK_MAX 8

struct transform {
	vec2i_t translate;
	bool flip_x;
};

struct transform_stack {
	struct transform stack[TRANSFORM_STACK_MAX];
	int top;
};

/// @brief Initializes a transform stack with sane default values.
///
/// @param s the stack to initialize
void transform_stack_init(struct transform_stack *s);

/// @brief Pushes a new transform onto the stack. The position is relative to
/// the topmost transform's position.
///
/// @param s the stack
/// @param t the transform to push
void transform_stack_push(struct transform_stack *s, struct transform t);

/// @brief Pops the topmost transform off the transform stack.
///
/// @param s the stack
void transform_stack_pop(struct transform_stack *s);

/// @brief Performs a transformed blit using the stack's topmost transform.
///
/// @param s the stack
/// @param source the source surface
/// @param dest the destination surface
/// @param destpos the position to blit to
void transformed_blit(struct transform_stack *s, struct surface *source, struct surface *dest,
                      vec2i_t destpos);

void transformed_fill(struct transform_stack *s, struct color_rgba color, struct surface *dest,
                      struct recti rect);

void transformed_draw_stroke_rect(struct transform_stack *s, struct color_rgba color,
                                  struct surface *dest, struct recti rect);

/// @brief Returns transform stack top position
vec2i_t transform_stack_get_translation(struct transform_stack *s);

#endif

/// @}
