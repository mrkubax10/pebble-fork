#include "color.h"

extern inline struct color_rgb color_rgb(uint8_t r, uint8_t g, uint8_t b);
extern inline struct color_rgba color_rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
