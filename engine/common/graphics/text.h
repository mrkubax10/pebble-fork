/// @addtogroup graphics
/// @{

#ifndef TEXT_H
#define TEXT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "color.h"

struct font *font_from_memory(void *data, size_t size);
void font_clean(struct font **font);

struct text_render_settings {
	struct color_rgb color;

	uint16_t height;

	bool outlined;
	struct color_rgb outline_color;
};

/// @brief Returns default values for text_render_settings (white text without
/// outline)
/// @return default values for text_render_settings
struct text_render_settings text_render_settings_defaults();

/// @brief Generate surface with ready-to-render text
/// @param utf8 UTF-8 encoded text
/// @param settings Render settings
/// @return surface
struct surface *text_render(struct font *font, const char *utf8,
                            struct text_render_settings *settings);

#endif

/// @}
