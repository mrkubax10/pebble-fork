#include "stb/stb_image.h"

#include "surface.h"

struct surface *
surface_from_memory(void *data, size_t size)
{
	int width = 0, height = 0;
	int channels_in_file = 0;
	stbi_uc *pixels = stbi_load_from_memory(data, size, &width, &height, &channels_in_file, 0);

	if (width <= 0 || height <= 0 || !pixels || channels_in_file < 3 || channels_in_file > 4) {
		if (pixels)
			free(pixels);
		return NULL;
	}

	struct surface *surface = surface_create_empty(
	    0, 0, channels_in_file == 3 ? SURFACE_PIXEL_FORMAT_RGB24 : SURFACE_PIXEL_FORMAT_RGBA32);

	// Directly modify pointer
	surface->pixels = pixels;
	surface->width = width;
	surface->height = height;
	surface->pitch = surface->width * channels_in_file;
	surface->pixel_format =
	    channels_in_file == 3 ? SURFACE_PIXEL_FORMAT_RGB24 : SURFACE_PIXEL_FORMAT_RGBA32;
	return surface;
}
