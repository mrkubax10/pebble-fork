#include "string.h"

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "common/util.h"

char *
strtolower(const char *str)
{
	if (!str)
		return NULL;

	// We should work on a copy
	char *ret = dstrcpy(str);

	for (int i = 0; ret[i]; i++) {
		ret[i] = tolower(str[i]);
	}

	return ret;
}

int
strendswith(const char *str, const char *suffix)
{
	if (str == NULL || suffix == NULL)
		return -1;

	size_t lenstr = (size_t)strlen(str);
	size_t lensuffix = strlen(suffix);

	if (lensuffix > lenstr)
		return -1;

	const char *strtocheck = (const char *)(str + lenstr - lensuffix);

	return strncmp(strtocheck, suffix, lensuffix) == 0;
}

// Thank you openbsd manual
int
strtoi(const char *str, int *dest)
{
	char *ep;

	errno = 0;
	long lval = strtol(str, &ep, 10);

	// Check if str is a number
	if (str[0] == '\0' || *ep != '\0')
		return -1;

	// Out of range?
	if ((errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN)) ||
	    (lval > INT_MAX || lval < INT_MIN))
		return -1;

	if (dest)
		*dest = lval;

	return 0;
}

char *
dsprintf(const char *format, ...)
{
	char *buf = NULL;
	int max_size = 0;

	va_list args_final;
	va_list args;
	va_start(args, format);

	// vsnprintf alters args, so we need to do a copy
	va_copy(args_final, args);

	int size = vsnprintf(NULL, 0, format, args) + 1;

	if (max_size < size) {
		if (buf)
			free(buf);
		max_size = size;
		buf = emalloc(max_size);
	}

	vsnprintf(buf, size, format, args_final);

	va_end(args);
	va_end(args_final);

	return buf;
}

char *
dstrcpy(const char *str)
{
	size_t size = (strlen(str) + 1) * sizeof(char);
	char *result = emalloc(size);
	memcpy(result, str, size);
	return result;
}
