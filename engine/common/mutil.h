/// @file mutil.h
/// @brief Abandon all hope — Ye Who Enter Here

#ifndef MUTIL_H
#define MUTIL_H

#ifndef static_assert
#define static_assert(x, msg) ((void)(struct { char z : (x) ? 1 : 0; } *)NULL)
#endif

#endif
