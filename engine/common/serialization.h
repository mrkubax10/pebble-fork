/// @addtogroup serialization Serialization & deserialization
/// @{

#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <stdint.h>
#include <stdio.h>

/// @brief Deserializes an 8-bit uint from the file.
/// @param f the file
/// @return the deserialized uint
inline uint8_t
fdeserialize_u8(FILE *f)
{
	uint8_t byte = 0;
	fread(&byte, sizeof(byte), 1, f);
	return byte;
}

/// @brief Deserializes a 16-bit uint from the file.
/// @param f the file
/// @return the deserialized uint
inline uint16_t
fdeserialize_u16(FILE *f)
{
	uint8_t bytes[2] = {0};
	fread(bytes, sizeof(bytes), 1, f);
	return (uint16_t)bytes[0] | ((uint16_t)bytes[1] << 8);
}

/// @brief Deserializes a 32-bit uint from the file.
/// @param f the file
/// @return the deserialized uint
inline uint32_t
fdeserialize_u32(FILE *f)
{
	uint8_t bytes[4] = {0};
	fread(bytes, sizeof(bytes), 1, f);
	return (uint32_t)bytes[0] | ((uint32_t)bytes[1] << 8) | ((uint32_t)bytes[2] << 16) |
	       ((uint32_t)bytes[3] << 24);
}

/// @brief Deserializes a 64-bit uint from the file.
/// @param f the file
/// @return the deserialized uint
inline uint64_t
fdeserialize_u64(FILE *f)
{
	uint8_t bytes[8] = {0};
	fread(bytes, sizeof(bytes), 1, f);
	return (uint64_t)bytes[0] | ((uint64_t)bytes[1] << 8) | ((uint64_t)bytes[2] << 16) |
	       ((uint64_t)bytes[3] << 24) | ((uint64_t)bytes[4] << 32) |
	       ((uint64_t)bytes[5] << 40) | ((uint64_t)bytes[6] << 48) | ((uint64_t)bytes[7] << 56);
}

/// @brief Serializes an 8-bit uint into the file.
/// @param f the file
/// @param x the uint to serialize
inline void
fserialize_u8(FILE *f, uint8_t x)
{
	fwrite(&x, sizeof(uint8_t), 1, f);
}

/// @brief Serializes a 16-bit uint into the file.
/// @param f the file
/// @param x the uint to serialize
inline void
fserialize_u16(FILE *f, uint16_t x)
{
	uint8_t bytes[2] = {
	    x & 0xFF,
	    x >> 8,
	};
	fwrite(bytes, sizeof(bytes), 1, f);
}

/// @brief Serializes a 32-bit uint into the file.
/// @param f the file
/// @param x the uint to serialize
inline void
fserialize_u32(FILE *f, uint32_t x)
{
	uint8_t bytes[4] = {
	    x & 0xFF,
	    (x >> 8) & 0xFF,
	    (x >> 16) & 0xFF,
	    (x >> 24) & 0xFF,
	};
	fwrite(bytes, sizeof(bytes), 1, f);
}

/// @brief Serializes a 64-bit uint into the file.
/// @param f the file
/// @param x the uint to serialize
inline void
fserialize_u64(FILE *f, uint64_t x)
{
	uint8_t bytes[8] = {
	    x & 0xFF,         (x >> 8) & 0xFF,  (x >> 16) & 0xFF, (x >> 24) & 0xFF,
	    (x >> 32) & 0xFF, (x >> 40) & 0xFF, (x >> 48) & 0xFF, (x >> 56) & 0xFF,
	};
	fwrite(bytes, sizeof(bytes), 1, f);
}

/// @brief Deserializes an 8-bit uint from the array at the position p,
///        then increments p.
/// @param a the array
/// @param the position to read from
/// @return the uint. p is set to the element after the uint
inline uint8_t
adeserialize_u8(const uint8_t a[], size_t *p)
{
	uint8_t r = a[*p];
	++*p;
	return r;
}

/// @brief Deserializes a 16-bit uint from the array at the position p,
///        then increments p.
/// @param a the array
/// @param the position to read from
/// @return the uint. p is set to the element after the uint
inline uint16_t
adeserialize_u16(const uint8_t a[], size_t *p)
{
	// can't use the trick from adeserialize_u8 because the order of operations
	// is undefined! thanks, C committee :)
	uint16_t r = (uint16_t)a[*p] | ((uint16_t)a[*p + 1] << 8);
	*p += 2;
	return r;
}

/// @brief Deserializes a 32-bit uint from the array at the position p,
///        then increments p.
/// @param a the array
/// @param the position to read from
/// @return the uint. p is set to the element after the uint
inline uint32_t
adeserialize_u32(const uint8_t a[], size_t *p)
{
	uint32_t r = (uint32_t)a[*p] | ((uint32_t)a[*p + 1] << 8) | ((uint32_t)a[*p + 2] << 16) |
	             ((uint32_t)a[*p + 3] << 24);
	*p += 4;
	return r;
}

/// @brief Deserializes a 64-bit uint from the array at the position p,
///        then increments p.
/// @param a the array
/// @param the position to read from
/// @return the uint. p is set to the element after the uint
inline uint64_t
adeserialize_u64(const uint8_t a[], size_t *p)
{
	uint64_t r = (uint64_t)a[*p] | ((uint64_t)a[*p + 1] << 8) | ((uint64_t)a[*p + 2] << 16) |
	             ((uint64_t)a[*p + 3] << 24) | ((uint64_t)a[*p + 4] << 32) |
	             ((uint64_t)a[*p + 5] << 40) | ((uint64_t)a[*p + 6] << 48) |
	             ((uint64_t)a[*p + 7] << 56);
	*p += 4;
	return r;
}

#endif

/// @}
