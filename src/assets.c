#include "assets.h"

#include "stb/stb_ds.h"

#include "assets_gc.h"
#include "backends/mixed/messagebox.h"
#include "common/graphics/surface.h"
#include "common/graphics/text.h"
#include "common/util.h"
#include "game.h"
#include "map/location.h"
#include "misc/map.h"
#include "sys/fs.h"

#define IS_NOT(name, first, second) \
	(!(strcmp(name, first) == 0) && !(strcmp(name, second) == 0))

#define ASSETS_PATH(p) const char *path = assets_path(p)

struct assets g_assets;

static uint8_t *
read_whole_file(const char *path, long *out_size)
{
	FILE *f = fopen(path, "rb");
	if (!f)
		return NULL;
	fseek(f, 0, SEEK_END);
	long file_size = ftell(f);
	*out_size = file_size;
	fseek(f, 0, SEEK_SET);

	uint8_t *data = emalloc(file_size);
	fread(data, 1, file_size, f);
	fclose(f);

	return data;
}

static struct surface *
assets_surface_from_file(const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	long out_size;
	uint8_t *data = read_whole_file(path, &out_size);
	if (!data) {
		messageboxf("Assets error", "Cannot open file %s", path);
		exit(1);
	}

	struct surface *surface = surface_from_memory(data, out_size);
	free(data);

	if (!surface) {
		messageboxf("Assets error", "Cannot decode image %s", path);
		exit(1);
	}

	return surface;
}

static struct surface *
assets_surface_from_gamedata(const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	struct gd_file *f = gamedata_request(g_assets.gamedata, path);
	if (!f) {
		messageboxf("Assets error", "Cannot open file %s", path);
		exit(1);
	}

	struct surface *surface = surface_from_memory(f->content.data, f->content.size);
	if (!surface) {
		messageboxf("Assets error", "Cannot decode image %s", path);
		exit(1);
	}

	return surface;
}

static struct font *
assets_font_from_file(const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	long out_size;
	uint8_t *data = read_whole_file(path, &out_size);
	if (!data) {
		messageboxf("Assets error", "Cannot open file %s", path);
		exit(1);
	}

	struct font *font = font_from_memory(data, out_size);
	free(data);

	if (!font) {
		messageboxf("Assets error", "Cannot decode font %s", path);
		exit(1);
	}

	return font;
}

static struct font *
assets_font_from_gamedata(const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	struct gd_file *f = gamedata_request(g_assets.gamedata, path);
	if (!f) {
		messageboxf("Assets error", "Cannot open file %s", path);
		exit(1);
	}

	struct font *font = font_from_memory(f->content.data, f->content.size);
	if (!font) {
		messageboxf("Assets error", "Cannot decode font %s", path);
		exit(1);
	}

	return font;
}

static struct audio_music *
assets_music_from_file(struct audio_context *context, const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	return audio_music_load_opus(context, path);
}

static struct audio_music *
assets_music_from_gamedata(struct audio_context *context, const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	switch (g_assets.gamedata->mode) {
	case GD_MODE_STATIC: {
		struct gd_file *f = gamedata_request(g_assets.gamedata, path);
		if (!f) {
			messageboxf("Assets error", "Cannot open file %s", path);
			exit(1);
		}

		return audio_music_from_memory_opus(context, f->content.data, f->content.size);
	}

	case GD_MODE_DYNAMIC: {
		struct gd_pointer ptr = gamedata_open(g_assets.gamedata, path);
		if (!GD_POINTER_EXISTS(ptr)) {
			messageboxf("Assets error", "Cannot open file %s", path);
			exit(1);
		}

		return audio_music_from_gamedata_opus(context, g_assets.gamedata, ptr);
	}
	}

	return NULL;
}

static struct audio_chunk *
assets_audio_from_file(struct audio_context *context, const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	return audio_chunk_load_opus(context, path);
}

static struct audio_chunk *
assets_audio_from_gamedata(struct audio_context *context, const char *p)
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, p);

	switch (g_assets.gamedata->mode) {
	case GD_MODE_STATIC: {
		struct gd_file *f = gamedata_request(g_assets.gamedata, path);
		if (!f) {
			messageboxf("Assets error", "Cannot open file %s", path);
			exit(1);
		}

		return audio_chunk_from_memory_opus(context, f->content.data, f->content.size);
	}

	case GD_MODE_DYNAMIC: {
		struct gd_pointer ptr = gamedata_open(g_assets.gamedata, path);
		if (!GD_POINTER_EXISTS(ptr)) {
			messageboxf("Assets error", "Cannot open file %s", path);
			exit(1);
		}

		return audio_chunk_from_gamedata_opus(context, g_assets.gamedata, ptr);
	}
	}

	return NULL;
}

struct surface *(*assets_load_surface)(const char *) = assets_surface_from_file;
struct font *(*assets_load_font)(const char *) = assets_font_from_file;
struct audio_music *(*assets_load_music)(struct audio_context *,
                                         const char *) = assets_music_from_file;
struct audio_chunk *(*assets_load_audio)(struct audio_context *,
                                         const char *) = assets_audio_from_file;

static const char *player_animation_directories[PLAYER_ANIMATION__LAST] = {
    [PLAYER_STAND] = "stand", [PLAYER_RUN] = "run",     [PLAYER_VELOCITY] = "velocity",
    [PLAYER_FALL] = "fall",   [PLAYER_SHOOT] = "shoot",
};

static const int player_animation_lengths[PLAYER_ANIMATION__LAST] = {
    [PLAYER_STAND] = 8, [PLAYER_RUN] = 3,   [PLAYER_VELOCITY] = 4,
    [PLAYER_FALL] = 5,  [PLAYER_SHOOT] = 4,
};

static void
assets_load_player_sprites(const char *dir)
{
	const char *fmt = "%s/%s/%d.png";
	char path[PATH_MAX_SIZE];

	for (int anim = 0; anim < PLAYER_ANIMATION__LAST; ++anim) {
		const char *subdir = player_animation_directories[anim];
		for (int i = 0; i < player_animation_lengths[anim]; ++i) {
			snprintf(path, PATH_MAX_SIZE, fmt, dir, subdir, i);

			// player sprites use the alpha channel rather than #00FF00
			struct surface *surf = assets_load_surface(path);
			g_assets.player[anim][i] = surf;
			g_assets.player[anim][i + 1] = NULL; // sentinel
		}
	}

	g_assets.player_bullet = assets_load_surface("sprites/bob/bullet.png");
	surface_set_colorkey(g_assets.player_bullet, true, 0, 255, 0);
}

static void
assets_readonly_location_add(struct location *location)
{
	arrput(g_assets.readonly_locations, location);
}

static void
assets_locations_from_files()
{
	char path[PATH_MAX_SIZE];
	snprintf(path, PATH_MAX_SIZE, "%s/%s", g_assets.dir, "locations");

	struct fs_dir *dir = fs_walk_start(path);
	struct fs_file *f;

	while ((f = fs_walk_dir(dir)) != NULL) {
		// Ignore "." and ".."
		if (IS_NOT(f->name, ".", "..") && strendswith(f->name, ".pbmap")) {
			int x, y;
			// Get x and y
			sscanf(f->name, "%d,%d.pbmap", &x, &y);

			// +2, because '/' and `\0`
			char final[PATH_MAX_SIZE + 2];
			snprintf(final, sizeof(final), "%s/%s", path, f->name);

			struct location *loc = location_create();
			loc->pos = vec2i(x, y);

			location_load(loc, final);

			assets_readonly_location_add(loc);
		}
	}

	fs_walk_end(dir);
	dir = NULL;
}

static void
assets_locations_from_gamedata(struct gamedata *gamedata)
{
	size_t len = shlenu(gamedata->files);
	for (size_t i = 0; i < len; i++) {
		char *path = gamedata->files[i].key;
		if (!strendswith(path, ".pbmap"))
			continue;

		struct gd_file *file = gamedata_request(gamedata, path);
		if (!file)
			return;

		int x, y;
		// Get x and y
		sscanf(fs_path_filename(file->path), "%d,%d.pbmap", &x, &y);

		struct location *loc = location_create();
		loc->pos = vec2i(x, y);

		location_load_array(loc, (const uint8_t *)file->content.data, file->content.size);
		assets_readonly_location_add(loc);
	}
}

void
assets_init(const char *assets_dir, struct game *game, int flags)
{
	(void)game;

	g_assets.readonly_locations = NULL;
	g_assets.gamedata = NULL;
	g_assets.dir = assets_dir;

	// When ASSETS_GAMEDATA is set, load gamedata and use it
	if (flags & ASSETS_GAMEDATA) {
		g_assets.gamedata = emalloc(sizeof(struct gamedata));

		gamedata_from_file(g_assets.gamedata, GD_MODE_DYNAMIC, g_config.gamedata);
		assets_load_surface = assets_surface_from_gamedata;
		assets_load_font = assets_font_from_gamedata;
		assets_load_music = assets_music_from_gamedata;
		assets_load_audio = assets_audio_from_gamedata;
	}

	g_assets.gc_obstacles = gc_surface_list_init(OBSTACLES_COUNT);
	g_assets.gc_backgrounds = gc_surface_list_init(BACKGROUNDS_COUNT);

	g_assets.langselect = assets_load_surface("gui/lang_select/langselect.png");
	g_assets.langarrow = assets_load_surface("gui/lang_select/langarrow.png");
	surface_set_colorkey(g_assets.langarrow, true, 0, 255, 0);

	assets_load_player_sprites("sprites/bob");

	g_assets.menu_arrows = assets_load_surface("gui/main_menu/arrows.png");
	surface_set_colorkey(g_assets.menu_arrows, true, 0, 255, 0);

	g_assets.menu_background = assets_load_surface("gui/main_menu/background.png");
	g_assets.menu_foreground = assets_load_surface("gui/main_menu/foreground.png");
	g_assets.menu_pebble_team_logo =
	    assets_load_surface("gui/main_menu/pebble_team_presents.png");
	g_assets.menu_game_logo = assets_load_surface("gui/main_menu/bobs_adventure.png");
	g_assets.menu_plate = assets_load_surface("gui/main_menu/menuplate.png");
	g_assets.menu_plate_arrow = assets_load_surface("gui/main_menu/menuplate_arrow.png");

	g_assets.pause_play_menu_arrow = assets_load_surface("gui/pause_play_menu/arrow.png");

	g_assets.game_shadow_bottom = assets_load_surface("sprites/shadow_bottom.png");
	g_assets.game_shadow_right = assets_load_surface("sprites/shadow_right.png");
	g_assets.game_shadow_bottom_right = assets_load_surface("sprites/shadow_bottom_right.png");

	g_assets.savepoint_shine = assets_load_surface("sprites/savepoint_shine.png");

	g_assets.projectiles[0] = assets_load_surface("sprites/projectiles/1.png");
	g_assets.projectiles[6] = assets_load_surface("sprites/projectiles/7.png");

	g_assets.main_font = assets_load_font("ubuntu-bold.ttf");
	g_assets.ui_font = assets_load_font("geompixel.ttf");

	struct ui_style main_style;
	main_style.style = assets_load_surface("gui/ui.png");
	main_style.checkbox_checked = recti(0, 0, 6, 5);
	main_style.checkbox = recti(8, 0, 5, 5);
	g_assets.main_style = main_style;

	if (flags & ASSETS_GAMEDATA) {
		// Load locations from gamedata, if we are using it
		assets_locations_from_gamedata(g_assets.gamedata);
	} else {
		assets_locations_from_files();
	}
}

void
assets_clean()
{
	gc_surface_list_free(&g_assets.gc_obstacles);
	gc_surface_list_free(&g_assets.gc_backgrounds);

	for (int anim = 0; anim < PLAYER_ANIMATION__LAST; ++anim) {
		for (int i = 0; i < player_animation_lengths[anim]; ++i) {
			surface_clean(&g_assets.player[anim][i]);
		}
	}
	surface_clean(&g_assets.player_bullet);

	surface_clean(&g_assets.menu_arrows);
	surface_clean(&g_assets.menu_background);
	surface_clean(&g_assets.menu_foreground);
	surface_clean(&g_assets.menu_pebble_team_logo);
	surface_clean(&g_assets.menu_plate_arrow);
	surface_clean(&g_assets.menu_plate);
	surface_clean(&g_assets.langarrow);
	surface_clean(&g_assets.langselect);
	surface_clean(&g_assets.game_shadow_bottom);
	surface_clean(&g_assets.game_shadow_right);
	surface_clean(&g_assets.game_shadow_bottom_right);
	surface_clean(&g_assets.savepoint_shine);

	for (ptrdiff_t i = 0; i < arrlen(g_assets.readonly_locations); i++) {
		location_free(&g_assets.readonly_locations[i]);
	}

	arrfree(g_assets.readonly_locations);
	g_assets.readonly_locations = NULL;

	font_clean(&g_assets.main_font);

	// free(NULL) does nothing, so it is safe
	free(g_assets.gamedata);
}

struct surface *
assets_get_obstacle(int id)
{
	int index = id - 1;
	assert(index >= 0 && index < OBSTACLES_COUNT);

	struct surface *surf = gc_surface_list_get(g_assets.gc_obstacles, index);

	if (!surf) {
		char buf[1024];
		snprintf(buf, sizeof(buf), "obstacles/%d.png", id);
		surf = gc_surface_list_load(g_assets.gc_obstacles, index, buf);

		// Check if surface needs to be color-keyed
		bool needs_color_key = false;
		for (uint32_t y = 0; y < surf->height; y++) {
			for (uint32_t x = 0; x < surf->width; x++) {
				const uint8_t *rgb =
				    ((const uint8_t *)surf->pixels) + (y * surf->pitch + x * 3 /*Packed RGB*/);
				if (rgb[0] == 0 && rgb[1] == 255 && rgb[2] == 0) {
					needs_color_key = true;
					goto skip_loop;
				}
			}
		}
	skip_loop:
		if (needs_color_key) {
			surface_set_colorkey(surf, true, 0, 255, 0);
		}
	}

	assert(surf);
	return surf;
}

struct surface *
assets_get_background(int id)
{
	int index = id - 1;
	assert(index >= 0 && index < BACKGROUNDS_COUNT);

	struct surface *surf = gc_surface_list_get(g_assets.gc_backgrounds, index);

	if (!surf) {
		char buf[PATH_MAX_SIZE];
		snprintf(buf, sizeof(buf), "backgrounds/%d.png", id);

		surf = gc_surface_list_load(g_assets.gc_backgrounds, index, buf);
	}

	assert(surf);
	return surf;
}

void
assets_gc()
{
	gc_surface_list_gc(g_assets.gc_backgrounds);
	gc_surface_list_gc(g_assets.gc_obstacles);
}

struct audio_music *
assets_get_music(struct game *game, int id)
{
	int index = id - 1;

	struct audio_music *music = g_assets.music[index];
	if (!music) {
		char buf[PATH_MAX_SIZE];
		snprintf(buf, PATH_MAX_SIZE, "audio/soundtrack/%d.opus", id);

		g_assets.music[index] = assets_load_music(game->audio_ctx, buf);
		return g_assets.music[index];
	}

	return NULL;
}

void
assets_free_music(int id)
{
	struct audio_music **mus = &g_assets.music[id];
	if (*mus) {
		audio_music_clean(&*mus);
	}
}

void
assets_free_music_ptr(struct audio_music *mus)
{
	if (!mus)
		return;

	for (size_t i = 0; i < MUSIC_COUNT; i++) {
		if (g_assets.music[i] == mus) {
			assets_free_music(i);
		}
	}
}
