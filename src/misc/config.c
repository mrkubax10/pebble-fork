#include "config.h"

#include "assets.h"
#include "backends/mixed/messagebox.h"
#include "common/string.h"
#include "common/util.h"
#include "sys/fs.h"

struct conf g_config;

static int
handler(void *user, const char *section, const char *name, const char *value)
{
	int ret;
	struct conf *pconfig = (struct conf *)user;

	// Convert strings to lowercase
	char *lowersection = strtolower(section);
	char *lowername = strtolower(name);
	char *lowervalue = strtolower(value);

#define MATCH(s, n) strcmp(lowersection, s) == 0 && strcmp(lowername, n) == 0

	if (MATCH("options", "windowsize")) {
		// If value is invalid number, then window_size will be 0
		pconfig->window_size = 0;

		if (strtoi(value, &pconfig->window_size) < 0) {
			LOG_ERROR("Invalid window_size. Ignoring.");
		}
	} else if (MATCH("options", "assets")) {
		// Check if path to assets is valid
		if (!fs_is_valid_path(value)) {
			messagebox("Invalid path", "Path to assets is invalid");
			exit(1);
		}

		// For assets path, we must use value instead of lowervalue
		pconfig->assets_dir = dstrcpy(value);
	} else if (MATCH("options", "vsync")) {
		pconfig->vsync = true;

		if (strcmp(lowervalue, "false") == 0) {
			pconfig->vsync = false;
		}
	} else if (MATCH("options", "datapath")) {
		pconfig->data_path = dstrcpy(value);
	} else if (MATCH("options", "gamedata")) {
		if (!fs_is_valid_path(value)) {
			messagebox("Invalid path", "Path to gamedata is invalid");
			exit(1);
		}

		pconfig->gamedata = dstrcpy(value);
		;
	} else {
		ret = 0;
		goto g_free;
	}

	ret = 1;
g_free:
	free(lowersection);
	free(lowername);
	free(lowervalue);

	return ret;
}

int
parse_config(const char *path, struct conf *conf)
{
	assert(conf != NULL);
	conf->assets_dir = DEFAULT_ASSETS_DIR;
	conf->gamedata = NULL;
	conf->data_path = NULL;
	conf->vsync = true;
	conf->window_size = 0;
	return ini_parse(path, handler, conf);
}
