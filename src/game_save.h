/// @addtogroup game_save Game save
/// @{

#ifndef GAME_SAVE_H
#define GAME_SAVE_H

#include "common/math/vector.h"
#include "common/serialization.h"

// TODO(legalprisoner8140):
// Add version so we know how to read from savefile safely

/// Magic value. Start of game save.
#define GAME_SAVE_FILE_HEADER 938813

/// The path to which the save file should be written
#define GAME_SAVE_FILE_NAME   "savefile"

/// The structure containing the data that will be written to the file.
struct game_save {
	/// Checkpoint's global position
	vec2i_t checkpoint_position;

	/// The maximum amount of bullets that can be present on the screen at once
	uint8_t max_bullets;

	uint8_t keys_collected;
	uint8_t kolota_defeated;
};

/// @brief Creates new empty pointer to game_save struct
/// @return Created pointer to game_save struct
struct game_save *game_save_create_empty();

/// @brief Returns the path to the file.
///
/// This is a simple get_data_path() wrapper that also takes care of detecting 
/// when the data path is overwritten in the config.
/// Prefer it over get_data_path().
///
/// Example, where get_data_path() returns "/home/anon/.local/share/pebble/":
/// @code{.c}
/// // returns "/home/anon/.local/share/pebble/foo"
/// char *path = game_save_get_path("foo");
///
/// // remember to free it later!
/// free(path);
/// @endcode
///
/// @return local application directory + filename. Remember to free it after
/// you use it.
/// @see get_data_path()
char *game_save_get_path(const char *filename);

/// @brief Loads game save file.
///
/// Example:
/// @code{.c}
/// char *path = game_save_get_path("foo");
/// struct game_save *save = game_save_load_file(path);
/// free(path);
/// @endcode
///
/// @param path Path to save file
/// @return Created pointer to game_save struct or NULL if file not found or is invalid.
struct game_save *game_save_load_file(const char *path);

/// @brief Saves game_save data to file.
/// @param gs pointer to game_save struct
/// @param path Path
/// @note Function fails if path is invalid
void game_save_write_file(struct game_save *gs, const char *path);

/// @brief Clean.
/// @param gs pointer to game_save struct
void game_save_clean(struct game_save *gs);

#endif

/// @}
