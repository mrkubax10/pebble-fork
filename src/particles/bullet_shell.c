#include "bullet_shell.h"

#include <math.h>

#include "map/world.h"
#include "physics/collision_groups.h"
#include "physics/physics.h"
#include "sound_system.h"

static void
render(struct particle_bullet_shell *part, float step)
{
	(void)step;

	vec2i_t pos = world_get_visual_pos(part->world, part->position);

	surface_set_pixel_safe(part->world->game->screen, pos.x, pos.y, part->red, part->green,
	                       part->blue, 255);

	surface_set_pixel_safe(part->world->game->screen, pos.x + 1, pos.y, part->red, part->green,
	                       part->blue, 255);
}

static void
update(struct particle_bullet_shell *part)
{
	part->lifetime--;
	if (part->lifetime == 0) {
		part->remove = true;
		return;
	}

	if (!part->bouncing) {
		return;
	}

	part->position.x += part->velocity.x;
	part->position.y += part->velocity.y;

	part->body->position = part->position;

	bool center, left, right, top, bottom;
	particle_collision_get_flags(part->world, part->position, 3, &center, &left, &right, &top,
	                             &bottom);

	bool collided = particle_bounce_physics_tick(&part->velocity, center, left, right, top,
	                                             bottom, &part->bounce_count, 0.6f, 0.7f);

	if (collided) {
		int volume = 255 - (part->bounce_count - 1) * 64;
		if (volume > 0) {
			struct chunk_play_params params = chunk_play_params_defaults();
			params.volume = volume;
			sound_system_play_ex(part->world->game->sound_sys, SAMPLE_BULLET_SHELL, &params);
		}
	}

	if ((left && right && top && bottom) || part->bounce_count > 4) {
		part->bouncing = false;
	}
}

static void
clean(struct particle_bullet_shell *part)
{
	part->body->remove = true;
}

static void
collide_with_water(struct physics_body *b, struct obstacle o, vec2i_t oposition)
{
	(void)oposition;
	(void)o;

	struct particle_bullet_shell *part = b->user;
	part->velocity.y *= 0.9f;
	part->velocity.y -= 0.15f;
	part->velocity.x *= 0.95f;
}

struct particle_bullet_shell *
particle_bullet_shell_create(struct world *world, vec2_t position, vec2_t velocity,
                             uint8_t red, uint8_t green, uint8_t blue)
{
	struct particle_bullet_shell *part = ecalloc(1, sizeof(struct particle_bullet_shell));

	particle_init(part, world, position, render, update, clean);

	part->velocity = velocity;
	part->lifetime = 100 + rand() % 100;
	part->red = red;
	part->green = green;
	part->blue = blue;
	part->bouncing = true;

	part->body = physics_body_create(world->space);
	part->body->position = position;
	part->body->size = vec2(3.0f, 1.0f);
	part->body->user = part;

	physics_body_obstacle_callback(part->body, collide_with_water, OBSTACLE_COLLISION_WATER);

	return part;
}
