#include "bubble.h"

#include <math.h>

#include "common/graphics/surface.h"
#include "map/location.h"
#include "map/obstacle_data.h"

static void
render(struct particle_bubble *bubble, float step)
{
	(void)step;

	vec2i_t pos = world_get_visual_pos(bubble->world, bubble->position);

	uint8_t red = 255, green = 255, blue = 255, alpha = 255;
	struct surface *surf = bubble->world->game->screen;

	// Bubble
	if (!bubble->single_dot) {
		surface_set_pixel_safe(surf, pos.x - 1, pos.y, red, green, blue, alpha);
		surface_set_pixel_safe(surf, pos.x + 1, pos.y, red, green, blue, alpha);
		surface_set_pixel_safe(surf, pos.x, pos.y - 1, red, green, blue, alpha);
		surface_set_pixel_safe(surf, pos.x, pos.y + 1, red, green, blue, alpha);
	} else {
		surface_set_pixel_safe(surf, pos.x, pos.y + 1, red, green, blue, alpha);
	}
}

static void
update(struct particle_bubble *bubble)
{
	bubble->lifetime--;
	if (bubble->lifetime == 0) {
		bubble->remove = true;
		return;
	}

	struct obstacle *ob = world_get_obstacle(bubble->world, bubble->position);
	if (!ob) {
		bubble->remove = true;
		return;
	}

	bubble->position.x += bubble->velocity.x;
	bubble->position.y += bubble->velocity.y;

	bool center, left, right, top, bottom;
	particle_collision_get_flags(bubble->world, bubble->position, 3, &center, &left, &right,
	                             &top, &bottom);

	if (left && right && bottom && top) {
		// stuck inside block
		bubble->remove = true;
		return;
	}

	if (top) {
		// Roof bounce
		if (bubble->velocity.y < 0.0f) {
			bubble->velocity.y *= -0.8f;
		}
	}

	// If in water
	if (ob->id == ID_WATER) {
		bubble->velocity.y -= 0.1f;

		// Terminal velocity
		if (bubble->velocity.y < -1.0f)
			bubble->velocity.y = -1.0f;

		// Damping
		bubble->velocity.x *= 0.9f;
		bubble->velocity.y *= 0.7f;
	} else {
		bubble->velocity.y += 0.3f;
	}

	bubble->single_dot = ob->id != ID_WATER;

	if (bubble->single_dot) {
		// Move randomly in X-axis
		bubble->velocity.x += (rand() % 1000 - 500) * 0.0002f;
	}
}

struct particle_bubble *
particle_bubble_create(struct world *world, vec2_t position, vec2_t velocity)
{
	struct particle_bubble *bubble = ecalloc(1, sizeof(struct particle_bubble));

	particle_init(bubble, world, position, render, update, NULL);

	bubble->velocity = velocity;
	bubble->lifetime = 300 + rand() % 100;

	return bubble;
}
