#include "particle.h"

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>

#include "map/location.h"
#include "map/world.h"

void
particle_init__impl(struct particle *particle, struct world *world, vec2_t position,
                    particle_render_fn render_impl, particle_update_fn update_impl,
                    particle_clean_fn clean_impl)
{
	assert(particle != NULL);
	assert(world != NULL);
	particle->world = world;
	particle->position = position;
	particle->remove = false;
	particle->remove_offscreen = true;
	particle->clean_impl = clean_impl;
	particle->update_impl = update_impl;
	particle->render_impl = render_impl;
}

void
particle_render__impl(struct particle *particle, float step)
{
	assert(particle != NULL);

	if (particle->render_impl)
		particle->render_impl(particle, step);
}

void
particle_update__impl(struct particle *particle)
{
	assert(particle != NULL);

	if (particle->update_impl)
		particle->update_impl(particle);
}

void
particle_clean__impl(struct particle *particle)
{
	assert(particle != NULL);

	if (particle->clean_impl)
		particle->clean_impl(particle);

	free(particle);
}

void
particle_collision_get_flags(struct world *world, vec2_t position, float margin,
                             bool *center, bool *left, bool *right, bool *top, bool *bottom)
{
	struct obstacle *ob_center = world_get_obstacle(world, position);
	struct obstacle *ob_left = world_get_obstacle(world, add2v(position, vec2(-margin, 0.0f)));
	struct obstacle *ob_right = world_get_obstacle(world, add2v(position, vec2(margin, 0.0f)));
	struct obstacle *ob_top = world_get_obstacle(world, add2v(position, vec2(0.0f, -margin)));
	struct obstacle *ob_bottom = world_get_obstacle(world, add2v(position, vec2(0.0f, margin)));

	// Collision flags
	*center = ob_center && ob_center->flags & FLAGS_IS_SOLID;
	*left = ob_left && ob_left->flags & FLAGS_IS_SOLID;
	*right = ob_right && ob_right->flags & FLAGS_IS_SOLID;
	*top = ob_top && ob_top->flags & FLAGS_IS_SOLID;
	*bottom = ob_bottom && ob_bottom->flags & FLAGS_IS_SOLID;
}

bool
particle_bounce_physics_tick(vec2_t *velocity, bool center, bool left, bool right,
                             bool top, bool bottom, uint32_t *bounce_count, float elasticity,
                             float torque)
{
	bool collided = false;
	if (!center) {
		// No collision detected, add gravity
		velocity->y += 0.13f;
		return false;
	}

	if (left && right && bottom && top) { // stuck inside block
		if (bounce_count)
			(*bounce_count)++;
		return true;
	}

	if (velocity->y > 0.0f && !top) {
		velocity->y *= -elasticity;
		velocity->x *= torque;
		if (bounce_count)
			(*bounce_count)++;
		collided = true;
	}

	if (velocity->y < 0.0f && !bottom) {
		velocity->y *= -elasticity;
		velocity->x *= torque;
		if (bounce_count)
			(*bounce_count)++;
		collided = true;
	}

	if (velocity->x < 0.0f && !right) {
		velocity->x *= -elasticity;
		velocity->y *= torque;
		if (bounce_count)
			(*bounce_count)++;
		collided = true;
	}

	if (velocity->x > 0.0f && !left) {
		velocity->x *= -elasticity;
		velocity->y *= torque;
		if (bounce_count)
			(*bounce_count)++;
		collided = true;
	}

	return collided;
}
