#ifndef PARTICLE_H
#define PARTICLE_H

#include <stdbool.h>
#include <stdint.h>

#include "common/graphics/transform.h"
#include "common/math/vector.h"
#include "game.h"
#include "map/world.h"

struct particle;

typedef void (*particle_render_fn)(struct particle *, float /*step*/);
typedef void (*particle_update_fn)(struct particle *);
typedef void (*particle_clean_fn)(struct particle *);

#define PARTICLE_HEADER             \
	struct world *world;            \
	bool remove;                    \
	bool remove_offscreen;          \
	particle_render_fn render_impl; \
	particle_update_fn update_impl; \
	particle_clean_fn clean_impl;   \
	vec2_t position;

/// The particle header.
///
/// `remove` - signals the world to remove the particle after the current tick.
/// `remove_offscreen` - signals the world to remove the particle if it's
/// off-screen. Defaults to `true`.
struct particle {
	PARTICLE_HEADER
};

void particle_init__impl(struct particle *particle, struct world *world, vec2_t position,
                         particle_render_fn render_impl, particle_update_fn update_impl,
                         particle_clean_fn clean_impl);

void particle_render__impl(struct particle *particle, float step);

void particle_update__impl(struct particle *particle);

void particle_clean__impl(struct particle *particle);

#define particle_init(p, world, initial_pos, render_fn, update_fn, clean_fn)          \
	particle_init__impl((struct particle *)p, world, initial_pos,                     \
	                    (particle_render_fn)render_fn, (particle_update_fn)update_fn, \
	                    (particle_clean_fn)clean_fn)

#define particle_render(p, step) particle_render__impl((struct particle *)p, step)

#define particle_update(p) particle_update__impl((struct particle *)p)

#define particle_clean(p) particle_clean__impl((struct particle *)p);

/// @brief Get collision information at given position
/// @param margin Margin in pixels from center
/// @param center Set to true when {position.x, position.y} is in solid obstacle
/// @param left Set to true when {position.x - margin, position.y} is in solid obstacle
/// @param right Set to true when {position.x + margin, position.y} is in solid obstacle
/// @param top Set to true when {position.x, position.y - margin} is in solid obstacle
/// @param bottom Set to true when {position.x, position.y + margin} is in solid obstacle
void particle_collision_get_flags(struct world *world, vec2_t position, float margin,
                                  bool *center, bool *left, bool *right, bool *top,
                                  bool *bottom);

/// @brief Perform physics on velocity, based on collision flags
/// @param velocity Input velocity
/// @param bounce_count Optional, can be NULL
/// @returns true if collision happened
bool particle_bounce_physics_tick(vec2_t *velocity, bool center, bool left, bool right,
                                  bool top, bool bottom, uint32_t *bounce_count,
                                  float elasticity, float torque);

#endif // PARTICLE_H
