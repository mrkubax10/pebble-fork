#ifndef PART_BULLET_SHELL_H
#define PART_BULLET_SHELL_H

#include "particle.h"

struct particle_bullet_shell {
	PARTICLE_HEADER
	vec2_t velocity;
	uint16_t lifetime;
	uint8_t red;
	uint8_t green;
	uint8_t blue;
	bool bouncing;
	uint32_t bounce_count;

	struct physics_body *body;
};

struct particle_bullet_shell *particle_bullet_shell_create(struct world *world,
                                                           vec2_t position,
                                                           vec2_t velocity, uint8_t red,
                                                           uint8_t green, uint8_t blue);

#endif
