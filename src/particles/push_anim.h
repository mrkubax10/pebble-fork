#ifndef PUSH_ANIM_H
#define PUSH_ANIM_H

#include "particle.h"

struct particle_push_anim {
	PARTICLE_HEADER
	vec2_t target_pos;
	float anim;
	float anim_prev;
};

struct particle_push_anim *particle_push_anim_create(struct world *world, vec2_t position,
                                                     vec2_t target_position);

#endif
