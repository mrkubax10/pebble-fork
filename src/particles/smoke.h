#ifndef PART_COLORED_H
#define PART_COLORED_H

#include "particle.h"

struct particle_smoke {
	PARTICLE_HEADER
	vec2_t velocity;
	uint16_t set_lifetime;
	uint16_t remaining_lifetime;
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

struct particle_smoke *particle_smoke_create(struct world *world, vec2_t position,
                                             vec2_t velocity, uint8_t red, uint8_t green,
                                             uint8_t blue, uint16_t lifetime);

#endif
