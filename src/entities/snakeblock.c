#include "snakeblock.h"

#include <math.h>
#include <stdio.h>

#include "common/graphics/surface.h"
#include "common/log.h"
#include "map/obstacle_data.h"
#include "physics/collision_groups.h"
#include "player.h"

///
/// BLOCK
///

#define BLOCK_LIFE 64

struct block {
	ENTITY_HEADER

	int life;
	vec2_t pos;
	vec2i_t location_pos;
	struct obstacle *ob;
};

static void
b_clean(struct block *b)
{
	b->ob->flags &= ~FLAGS_IS_SOLID;
}

static void
b_render(struct block *b, struct surface *s, float step)
{
	(void)step;

	struct surface *sprite = assets_get_obstacle(ID_SNAKEBLOCK_BLOCK);

	transformed_blit(&b->world->game->tstack, sprite, s, to_vec2i(b->pos));

	// NOTE: we don't have anything like SDL_SetSurfaceColorMod so there goes this
	transformed_fill(&b->world->game->tstack, color_rgba(4 * b->life, 4 * b->life, 255, 128), s,
	                 recti(b->pos.x, b->pos.y, sprite->width, sprite->height));
}

static void
b_update(struct block *b)
{
	b->life--;

	if (b->life <= 0) {
		b->remove = true;
	}
}

static struct block *
block_create(struct world *world, vec2i_t location_pos, vec2_t global_pos)
{
	struct block *b = ecalloc(1, sizeof(*b));

	entity_init(b, world, b_render, b_update, b_clean);

	b->pos = global_pos;
	b->life = BLOCK_LIFE;

	vec2i_t pos = world_get_obstacle_pos_round(global_pos);

	struct location *loc = world_get_location(world, location_pos);
	if (!loc)
		return NULL;

	struct obstacle *ob = location_get_obstacle(loc, pos);
	if (!ob)
		return NULL;

	ob->flags ^= FLAGS_IS_SOLID;
	b->ob = ob;

	return b;
}

///
/// SNAKEBLOCK
///

#define SB_WAIT_TICKS 10

enum {
	DIRECTION_RIGHT,
	DIRECTION_DOWN,
	DIRECTION_LEFT,
	DIRECTION_UP,
};

static void
sb_clean(struct snakeblock *sb)
{
	sb->body->remove = true;
}

static void
sb_render(struct snakeblock *sb, struct surface *s, float step)
{
	(void)step;

	struct recti rect =
	    recti(sb->body->position.x, sb->body->position.y, OBSTACLE_SCALE, OBSTACLE_SCALE);

	uint8_t r = 25 * (sb->ticks % 10);
	uint8_t g = r;
	uint8_t b = r;

	transformed_fill(&sb->world->game->tstack, color_rgba(r, g, b, 255), s, rect);
}

static void
sb_update(struct snakeblock *sb)
{
	sb->ticks++;

	vec2_t *pos = &sb->body->position;

	vec2i_t obstacle_pos = world_get_pos_in_location(*pos);
	if (obstacle_pos.y < 5 || obstacle_pos.x < 5 || obstacle_pos.x >= 315 ||
	    obstacle_pos.y >= 315) {
		sb->remove = true;
	}

	switch (sb->direction) {
	case DIRECTION_RIGHT:
		pos->x += 1.0f;
		break;
	case DIRECTION_DOWN:
		pos->y += 1.0f;
		break;
	case DIRECTION_LEFT:
		pos->x -= 1.0f;
		break;
	case DIRECTION_UP:
		pos->y -= 1.0f;
		break;
	}

	struct obstacle *ob = world_get_obstacle(sb->world, *pos);
	if (!ob)
		return;

	switch (ob->id) {
	case ID_SNAKEBLOCK_RIGHT:
		sb->direction = DIRECTION_RIGHT;
		break;
	case ID_SNAKEBLOCK_DOWN:
		sb->direction = DIRECTION_DOWN;
		break;
	case ID_SNAKEBLOCK_LEFT:
		sb->direction = DIRECTION_LEFT;
		break;
	case ID_SNAKEBLOCK_UP:
		sb->direction = DIRECTION_UP;
		break;
	case ID_SNAKEBLOCK_STOP:
		sb->remove = true;
		break;
	}

	if (sb->ticks == SB_WAIT_TICKS * (sb->ticks / SB_WAIT_TICKS)) {
		struct block *b = block_create(sb->world, sb->location_pos, *pos);
		world_spawn_entity(b->world, (struct entity *)b);
	}
}

static void
sb_collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	struct snakeblock *sb = (struct snakeblock *)bp->user;
	struct player *player = (struct player *)bc->user;

	if (sb->direction == DIRECTION_UP) {
		player->body->position.y -= 1.25f;
		player->body->velocity.y = 0.0f;
	}
}

struct snakeblock *
snakeblock_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct snakeblock *sb = ecalloc(1, sizeof(*sb));

	entity_init(sb, world, sb_render, sb_update, sb_clean);

	vec2_t pos = world_get_global_pos(location_pos, obstacle_pos);

	sb->body = physics_body_create(world->space);
	sb->body->position = pos;
	sb->body->size = vec2(8, 4);
	sb->body->user = sb;

	sb->direction = DIRECTION_RIGHT;
	sb->ticks = 0;
	sb->location_pos = location_pos;
	sb->obstacle_pos = obstacle_pos;

	physics_body_body_callback(sb->body, sb_collide_with_player, BODY_COLLISION_PLAYER);

	// Due to the fact that entities load earlier than they should,
	// world_get_obstacle can fail and block will not be created.
	struct block *b = block_create(world, location_pos, pos);
	if (b)
		world_spawn_entity(world, (struct entity *)b);

	return sb;
}

///
/// SNAKE BLOCK GENERATOR
///

#define SBG_WAIT_TICKS 240

static void
sbg_update(struct sb_generator *sbg)
{
	sbg->ticks++;

	if (sbg->ticks == SBG_WAIT_TICKS * (sbg->ticks / SBG_WAIT_TICKS)) {
		struct snakeblock *sb =
		    snakeblock_create(sbg->world, sbg->location_pos, sbg->obstacle_pos);
		world_spawn_entity(sbg->world, (struct entity *)sb);
	}
}

struct sb_generator *
sb_generator_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct sb_generator *sbg = ecalloc(1, sizeof(*sbg));

	entity_init(sbg, world, NULL, sbg_update, NULL);

	sbg->location_pos = location_pos;
	sbg->obstacle_pos = obstacle_pos;
	sbg->ticks = 0;

	return sbg;
}
