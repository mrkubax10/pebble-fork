#include "entities/player.h"

#include <math.h>
#include <stddef.h>
#include "stb/stb_ds.h"

#include "assets.h"
#include "backends/render/input_base.h"
#include "entities/util.h"
#include "map/obstacle_data.h"
#include "particles/bubble.h"
#include "particles/bullet_shell.h"
#include "particles/colored.h"
#include "physics/collision_groups.h"
#include "projectile.h"
#include "sound_system.h"

static inline struct surface *
get_animation_sprite(struct player *p)
{
	return g_assets.player[p->animation][p->animation_frame];
}

static void
render(struct player *p, struct surface *s, float step)
{
	struct game *game = p->world->game;

	transform_stack_push(&game->tstack, (struct transform){
	                                        .flip_x = p->facing == FACING_LEFT,
	                                    });
	entity_util_draw_sprite(game, s, p->body, get_animation_sprite(p), vec2i(0, 0), step);
	transform_stack_pop(&game->tstack);
}

// returns whether the bullet was shot (there was a free slot)
static inline bool
shoot(struct player *p)
{
	int slot = 0;
	while (slot < p->max_bullets && p->bullets[slot] != NULL) {
		++slot;
	}
	if (slot == p->max_bullets)
		return false;

	vec2_t velocity = mul2f(vec2(5, 0), (p->facing == FACING_RIGHT) * 2 - 1);
	vec2_t position = add2v(p->body->position, velocity);

	position.x += 5;
	position.y += 8;

	struct projectile *pr =
	    projectile_create(p->world, position, velocity, vec2(5, 5), PROJECTILE_TARGET_ENEMIES,
	                      g_assets.player_bullet);
	// we override this check so as to implement the max bullet count on screen
	// limit from original PB
	pr->remove_offscreen = false;

	p->bullets[slot] = pr;
	world_spawn_entity(p->world, (struct entity *)pr);

	return true;
}

static inline void
check_bullets(struct player *p)
{
	vec2i_t loc_pos;
	player_get_location_pos(p, &loc_pos);

	struct rectf viewport = {
	    .x = loc_pos.x * LOCATION_WIDTH * OBSTACLE_SCALE,
	    .y = loc_pos.y * LOCATION_HEIGHT * OBSTACLE_SCALE,
	    .w = LOCATION_WIDTH * OBSTACLE_SCALE,
	    .h = LOCATION_HEIGHT * OBSTACLE_SCALE,
	};

	for (int i = 0; i < p->max_bullets; ++i) {
		struct projectile *projectile = p->bullets[i];
		if (projectile == NULL)
			continue;

		struct rectf hitbox = physics_body_hitbox(projectile->body);
		if (!rintersect(&viewport, &hitbox)) {
			projectile->remove = true;
			p->bullets[i] = NULL;
			continue;
		}

		// failsafe
		if (projectile->age >= projectile->lifetime - 1) {
			p->bullets[i] = NULL;
		}
	}
}

#define EPSILON 0.001f

static inline void
update_movement(struct player *p)
{
	struct game *game = p->world->game;
	struct input *input = &game->input;
	struct physics_body *body = p->body;

	// left and right movement
	// bob doesn't have any acceleration or deceleration time, he starts
	// and stops instantly and moves 2px per frame
	if (!p->is_in_kinetic && !p->is_in_water)
		body->velocity.x = 0;

	bool pressing_side = false;

	if (input_key(input, KEY_LEFT, STATE_IS_DOWN)) {
		if (!p->is_in_water) {
			body->velocity.x -= 2.0f;
		} else {
			p->virtual_velocity.x -= 0.05f;
		}
		pressing_side = true;
		p->facing = FACING_LEFT;
	}
	if (input_key(input, KEY_RIGHT, STATE_IS_DOWN)) {
		if (!p->is_in_water) {
			body->velocity.x += 2;
		} else {
			p->virtual_velocity.x += 0.05f;
		}
		pressing_side = true;
		p->facing = FACING_RIGHT;
	}

	if (pressing_side && game->total_ticks % 14 == 0 &&
	    physics_body_collision(body, WALL_TOP, IS_COLLIDING)) {
		enum sample_name name;
		switch (rand() % 3) {
		case 0:
			name = SAMPLE_WALK1;
			break;
		case 1:
			name = SAMPLE_WALK2;
			break;
		case 2:
			name = SAMPLE_WALK3;
			break;
		}
		sound_system_play(game->sound_sys, name);
	}

	// jumping
	if (input_key(input, KEY_JUMP, STATE_IS_DOWN) &&
	    physics_body_collision(body, WALL_TOP, IS_COLLIDING)) {
		if (body->force.y >= 0.0f - EPSILON) {
			sound_system_play(game->sound_sys, SAMPLE_JUMP);
		}
		body->force.y -= 3.8;
	}
	if (input_key(input, KEY_JUMP, STATE_JUST_RELEASED) && body->velocity.y < 0) {
		body->velocity.y = 0;
	}

	// Water
	if (input_key(input, KEY_JUMP, STATE_IS_DOWN) && p->is_in_water) {
		body->velocity.y -= 0.75f;
	}

	if (pressing_side && physics_body_collision(body, WALL_TOP, IS_COLLIDING)) {
		// Generate walking particles
		vec2_t part_pos;
		part_pos.x = body->position.x + 8;
		part_pos.y = body->position.y + 16;
		vec2_t part_vel;
		part_vel.y = -1.2f + (rand() % 1000 - 500) * 0.001f;
		part_vel.x =
		    0.7f * (p->facing == FACING_LEFT ? 1.0f : -1.0f) + (rand() % 1000 - 500) * 0.001f;

		uint8_t red = 0, green = 0, blue = 0, alpha = 0;

		// Pick color from the screen
		vec2i_t visual_pos = world_get_visual_pos(p->world, part_pos);
		visual_pos.y += 2; // grab color 2 pixels below player
		surface_get_pixel_safe(game->screen, visual_pos.x, visual_pos.y, &red, &green, &blue,
		                       &alpha);

		world_spawn_particle(
		    p->world, particle_colored_create(p->world, part_pos, part_vel, red, green, blue));
	}

	// kinetic damping
	if (p->is_in_kinetic) {
		body->velocity.x *= 0.92;
		body->velocity.y *= 0.98;
	}

	// Used in original PB, used also for water movement physics
	body->velocity.x += p->virtual_velocity.x;
	body->velocity.y += p->virtual_velocity.y;

	// Virtual velocity damping (code from original PB)
	p->virtual_velocity.x *= 0.965f;
	p->virtual_velocity.y *= 0.965f;

	// water physics
	if (p->is_in_water) {
		// damping
		body->velocity.x *= 0.6f;
		body->velocity.y *= 0.7f;

		body->velocity.y += 0.2f;

		// Generate bubbles
		if (rand() % 10 == 0) {
			vec2_t pos = p->body->position;
			pos.x += 8.0f;
			pos.y += 4.0f;
			world_spawn_particle(p->world,
			                     particle_bubble_create(p->world, pos, vec2(0.0f, 0.0f)));
		}
	}

	// Water (in and out sound)
	if (p->is_in_water != p->is_in_water_prev) {
		p->is_in_water_prev = p->is_in_water;

		if (p->is_in_water) {
			sound_system_play(game->sound_sys, SAMPLE_SPLASH_IN);
			p->body->velocity.y += 3.5f;

			// Generate bubble particles
			vec2_t pos = p->body->position;
			pos.y += 16.0f;
			pos.x += 8.0f;

			for (uint32_t i = 0; i < 50; i++) {
				// Bubble velocity
				vec2_t vel;
				vel.x = (rand() % 1000 - 500) * 0.003f + p->body->velocity.x;
				vel.y = (rand() % 1000) * 0.008f;
				world_spawn_particle(p->world, particle_bubble_create(p->world, pos, vel));
			}
		} else {
			// Generate bubble particles
			vec2_t pos = p->body->position;
			pos.y += 16.0f;
			pos.x += 8.0f;

			for (uint32_t i = 0; i < 50; i++) {
				// Bubble velocity
				vec2_t vel;
				vel.x = (rand() % 1000 - 500) * 0.003f + p->body->velocity.x;
				vel.y = (rand() % 1000) * -0.008f;
				world_spawn_particle(p->world, particle_bubble_create(p->world, pos, vel));
			}

			sound_system_play(game->sound_sys, SAMPLE_SPLASH_OUT);
			p->body->velocity.y -= 2.0f;
			p->body->velocity.x += 10.0f * (p->facing == FACING_LEFT ? -1.0f : 1.0f);
		}
	}

	// terminal velocity
	body->velocity.x = clampf(body->velocity.x, -3, 3);
	body->velocity.y = fmin(body->velocity.y, 8);

	// the ever-present force of gravity
	if (!p->is_in_water) {
		body->force.y += 0.2;
	}
}

static inline void
update_movement_flyhack(struct player *p)
{
	struct input *input = &p->world->game->input;
	struct physics_body *body = p->body;

	const int flyhack_speed = 4;

	if (input_key(input, KEY_LEFT, STATE_IS_DOWN)) {
		body->position.x -= flyhack_speed;
		p->facing = FACING_LEFT;
	}
	if (input_key(input, KEY_RIGHT, STATE_IS_DOWN)) {
		body->position.x += flyhack_speed;
		p->facing = FACING_RIGHT;
	}
	if (input_key(input, KEY_UP, STATE_IS_DOWN)) {
		body->position.y -= flyhack_speed;
	}
	if (input_key(input, KEY_DOWN, STATE_IS_DOWN)) {
		body->position.y += flyhack_speed;
	}
}

static inline void
shooting(struct player *p)
{
	struct game *game = p->world->game;
	struct input *input = &game->input;

	if (input_key(input, KEY_SHOOT, STATE_JUST_PRESSED)) {
		if (shoot(p)) {
			sound_system_play(game->sound_sys, SAMPLE_SHOOT);
			p->shooting_animation_timer = 4 * 4;

			// Generate "bullet shell" particle
			vec2_t pos = p->body->position;
			pos.x += 8;
			pos.y += 12;

			// Bullet velocity
			vec2_t vel;
			vel.x = -0.8f * ((p->facing == FACING_LEFT) ? -1.0f : 1.0f) +
			        (rand() % 100 - 50) * 0.003f;
			vel.y = -0.3f + (rand() % 100 - 50) * 0.003f;

			world_spawn_particle(p->world,
			                     particle_bullet_shell_create(p->world, pos, vel, 255, 255, 0));

			// Water knockback
			if (p->is_in_water) {
				p->body->velocity.x += 3.0f * (p->facing == FACING_LEFT ? 1.0f : -1.0f);
			}
		}
	}
}

static inline void
update_animation_timers(struct player *p, enum player_animation old)
{
	if (p->animation != old) {
		p->animation_frame = 0;
		p->animation_timer = 0;
	}
}

static inline void
animation_frames(struct player *p, int frametime, int loop_to)
{
	if (p->animation_timer > frametime) {
		p->animation_timer = 0;
		++p->animation_frame;
		if (get_animation_sprite(p) == NULL) {
			p->animation_frame = loop_to;
		}
	}
}

static inline void
update_animation(struct player *p)
{
	struct physics_body *body = p->body;

	// STATES
	enum player_animation old = p->animation;

	if (p->is_in_water) {
		if (body->velocity.y < 0.5f) {
			p->animation = PLAYER_VELOCITY;
		} else {
			p->animation = PLAYER_FALL;
		}
	} else {
		if (body->velocity.x < -0.25f || body->velocity.x > 0.25f) {
			// running
			p->animation = PLAYER_RUN;
		} else if (physics_body_collision(body, WALL_TOP, IS_COLLIDING))
			// standing/jumping
			p->animation = PLAYER_STAND;
		if (body->velocity.y < -EPSILON ||
		    (body->velocity.y > EPSILON && body->velocity.y < 2)) {
			p->animation = PLAYER_VELOCITY;
		}
		if (body->velocity.y > 2) {
			p->animation = PLAYER_FALL;
		}
	}

	// shooting
	if (p->shooting_animation_timer > 0) {
		p->animation = PLAYER_SHOOT;
		--p->shooting_animation_timer;
	}

	update_animation_timers(p, old);

	// FRAMES

	++p->animation_timer;

	switch (p->animation) {
	case PLAYER_STAND:
		animation_frames(p, 8, 0);
		break;
	case PLAYER_RUN:
		animation_frames(p, 3, 0);
		break;
	case PLAYER_VELOCITY:
		// the jumping animation is handled a bit differently because it's
		// dependent on the player's velocity
		if (body->velocity.y < -2.5f)
			p->animation_frame = 0;
		else if (body->velocity.y >= -2.5f && body->velocity.y < -EPSILON)
			p->animation_frame = 1;
		else if (body->velocity.y >= EPSILON && body->velocity.y < 1.1f)
			p->animation_frame = 2;
		else if (body->velocity.y >= 1.1f)
			p->animation_frame = 3;

		break;
	case PLAYER_FALL:
		animation_frames(p, 5, 0);
		break;
	case PLAYER_SHOOT:
		p->animation_frame = 3 - p->shooting_animation_timer / 4;
		break;

	case PLAYER_ANIMATION__LAST:
		// this value is not a valid animation state
		assert(false);
		break;
	}
}

static void
update(struct player *p)
{
	if (!CHEAT_ENABLED(p->cheats, CHEAT_FLYHACK))
		update_movement(p);
	else
		update_movement_flyhack(p);

	shooting(p);
	update_animation(p);
	check_bullets(p);
	p->is_in_kinetic = false;
	p->is_in_water = false;
}

static void
clean(struct player *p)
{
	p->body->remove = true;

	// Remove player from list
	for (ptrdiff_t i = 0; i < arrlen(p->world->players); i++) {
		if (p->world->players[i] == p) {
			arrdel(p->world->players, i);
			break;
		}
	}
}

void
player_kill(struct player *p)
{
	if (CHEAT_ENABLED(p->cheats, CHEAT_GODMODE))
		return;

	p->body->position = p->respawn_point;
	p->body->position_prev = p->body->position;
	p->body->velocity = vec2(0, 0);
	p->body->force = vec2(0, 0);
}

static void
collide_with_deadly(struct physics_body *b, struct obstacle o, vec2i_t oposition)
{
	(void)o;
	(void)oposition;

	struct player *p = (struct player *)b->user;

	int margin_left = 4;
	int margin_right = 6;
	if (b->position.x + b->size.x > oposition.x * OBSTACLE_SCALE + margin_left &&
	    b->position.x < oposition.x * OBSTACLE_SCALE + margin_right) {
		player_kill(p);
	}
}

static void
collide_with_kinetic(struct physics_body *b, struct obstacle o, vec2i_t oposition)
{
	(void)oposition;

	struct player *p = (struct player *)b->user;
	p->is_in_kinetic = true;

	switch (o.id) {
	case ID_KINETIC_RIGHT:
		b->force.x += 0.5;
		break;
	case ID_KINETIC_DOWN:
		b->force.y += 0.5;
		break;
	case ID_KINETIC_LEFT:
		b->force.x -= 0.5;
		break;
	case ID_KINETIC_UP:
		b->force.y -= 0.5;
		break;
	}
}

static void
collide_with_water(struct physics_body *b, struct obstacle o, vec2i_t oposition)
{
	(void)oposition;
	(void)o;

	struct player *p = (struct player *)b->user;
	p->is_in_water = true;
}

static void
collide_with_mover(struct physics_body *b, struct obstacle o, vec2i_t oposition)
{
	(void)oposition;

	switch (o.id) {
	case ID_MOVER_UP:
		b->position.y -= 3;
		break;
	case ID_MOVER_RIGHT:
		b->position.x += 3;
		break;
	case ID_MOVER_DOWN:
		b->position.y += 3;
		break;
	case ID_MOVER_LEFT:
		b->position.x -= 3;
		break;
	}
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct player *pl = (struct player *)bp->user;
	struct projectile *pr = (struct projectile *)bc->user;

	if (!(pr->target == PROJECTILE_TARGET_PLAYER))
		return;

	pr->age = pr->lifetime;

	player_kill(pl);
}

struct player *
player_create(struct world *world, vec2_t position)
{
	struct player *p = ecalloc(1, sizeof(*p));

	entity_init(p, world, render, update, clean);

	p->remove_offscreen = false;

	p->body = physics_body_create(world->space);
	p->body->position = position;
	p->body->size = vec2(16, 16);
	p->body->group = GROUP_PLAYER;
	p->body->collision_callback_mask = BODY_COLLISION_PLAYER;
	p->body->user = p;
	p->respawn_point = position;
	p->max_bullets = 8;
	p->cheats = CHEAT_NOHACK;

	physics_body_obstacle_callback(p->body, collide_with_deadly, OBSTACLE_COLLISION_DEADLY);
	physics_body_obstacle_callback(p->body, collide_with_kinetic, OBSTACLE_COLLISION_KINETIC);
	physics_body_obstacle_callback(p->body, collide_with_water, OBSTACLE_COLLISION_WATER);
	// this *was* on because that's supposedly how it works in PB, however,
	// pebble's engine is quite different and it causes the player to go so
	// fast they sometimes clip through the ceiling.
	// ->prevent_duplicate_calls = false;
	physics_body_obstacle_callback(p->body, collide_with_mover, OBSTACLE_COLLISION_MOVER);

	physics_body_body_callback(p->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	arrput(world->players, p);

	LOG_INFOF("player created at (%f, %f)", position.x, position.y);

	return p;
}

void
player_set_pos(struct player *p, vec2i_t pos)
{
	vec2_t *position = &p->body->position;

	position->x = pos.x;
	position->y = pos.y;
}

void
player_get_location_pos(struct player *p, vec2i_t *out_pos)
{
	vec2_t position = add2v(p->body->position, div2f(p->body->size, 2));
	out_pos->x = position.x / LOCATION_WIDTH / OBSTACLE_SCALE;
	out_pos->y = position.y / LOCATION_HEIGHT / OBSTACLE_SCALE;
}
