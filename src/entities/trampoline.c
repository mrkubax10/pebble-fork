#include "trampoline.h"

#include "map/obstacle_data.h"
#include "physics/collision_groups.h"
#include "player.h"

static void
clean(struct trampoline *t)
{
	t->body_top->remove = true;
}

static void
render(struct trampoline *t, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_TRAMPOLINE);
	entity_util_draw_sprite(t->world->game, s, t->body_top, sprite, vec2i(0, -t->offsetY), step);
}

static void
update(struct trampoline *t)
{
	(void)t;
}

static void
collide_with_player_top(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;

	p->body->velocity.y *= -1.2f;

	if (p->body->velocity.y < -7.0f) {
		p->body->velocity.y = -7.0f;
	}
}

static void
collide_with_player_bottom(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;

	if (p->body->velocity.y > 0.0f)
		return;

	p->body->velocity.y *= -0.8f;
}

struct trampoline *
trampoline_create(struct world *world, vec2_t global_pos)
{
	struct trampoline *t = ecalloc(1, sizeof(*t));

	entity_init(t, world, render, update, clean);	

	t->body_top = physics_body_create(world->space);
	t->body_top->position = global_pos;
	t->body_top->size = vec2(20, 5);
	t->body_top->user = t;
	
	t->body_bottom = physics_body_create(world->space);
	global_pos.y += 5.0f;
	t->body_bottom->position = global_pos;
	t->body_bottom->size = vec2(20, 5);
	t->body_bottom->user = t;

	t->offsetY = 0;

	physics_body_body_callback(t->body_top, collide_with_player_top, BODY_COLLISION_PLAYER);
	physics_body_body_callback(t->body_bottom, collide_with_player_bottom, BODY_COLLISION_PLAYER);

	return t;
}
