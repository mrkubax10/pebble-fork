#include "skeleton.h"

#include "map/obstacle_data.h"
#include "map/world.h"
#include "physics/collision_groups.h"
#include "projectile.h"

static void
clean(struct skeleton *s)
{
	s->body_bottom->remove = true;
}

static void
render(struct skeleton *s, struct surface *dest, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_SKELETON);
	entity_util_draw_sprite(s->world->game, dest, s->body_top, sprite, vec2i(0, 0), step);
}

static void
update(struct skeleton *s)
{
	if (s->goright) {
		s->body_bottom->velocity.x = 1.0f;
	} else {
		s->body_bottom->velocity.x = -1.0f;
	}

	// The head and bottom have separate collisions. If the head touches the obstacle, 
	// then you have to change direction. If the bottom touches it, it must jump.
	if (physics_body_collision(s->body_top, WALL_LEFT, IS_COLLIDING)) {
		s->goright = false;
	} else if (physics_body_collision(s->body_top, WALL_RIGHT, IS_COLLIDING)) {
		s->goright = true;
	} else if (physics_body_collision(s->body_bottom, WALL_LEFT, IS_COLLIDING) ||
	           physics_body_collision(s->body_bottom, WALL_RIGHT, IS_COLLIDING)) {
		s->body_bottom->position.y -= 1.0f;
		s->body_bottom->velocity.y = -2.0f;
	}

	if (physics_body_collision(s->body_top, WALL_BOTTOM, IS_COLLIDING)) {
		if (s->body_bottom->velocity.y < 0.0f) {
			s->body_bottom->velocity.y = 0.5f;
		}

		s->body_bottom->position.y += 5.0f;
	}

	vec2i_t in_location_pos = world_get_pos_in_location(s->body_bottom->position);

	if (in_location_pos.x < 15.0f) {
		s->goright = true;
	} else if (in_location_pos.x > 305.0f) {
		s->goright = false;
	}

	s->body_bottom->velocity.y += 0.25f;

	s->body_top->position.x = s->body_bottom->position.x;
	s->body_top->position.y = s->body_bottom->position.y - 10.0f;
	s->body_top->velocity = s->body_bottom->velocity;
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bc;

	struct skeleton *s = (struct skeleton *)bp->user;
	s->body_bottom->velocity.y = -5.0f;
	s->body_top->velocity.y = -5.0f;
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct skeleton *s = (struct skeleton *)bp->user;
	struct projectile *p = (struct projectile *)bc->user;

	if (p->target != PROJECTILE_TARGET_ENEMIES)
		return;

	// Reflect projectile
	p->body->velocity.x = -p->body->velocity.x;
	p->body->velocity.y = -2.0f;

	s->body_bottom->velocity.y = -2.0f;
	s->body_top->velocity.y = -2.0f;
}

struct skeleton *
skeleton_create(struct world *world, vec2_t global_pos)
{
	struct skeleton *s = ecalloc(1, sizeof(*s));

	entity_init(s, world, render, update, clean);

	s->body_top = physics_body_create(world->space);
	s->body_top->position = global_pos;
	s->body_top->size = vec2(10.0f, 10.0f);
	s->body_top->user = s;

	s->body_bottom = physics_body_create(world->space);
	global_pos.y += 10.0f;
	s->body_bottom->position = global_pos;
	s->body_bottom->size = vec2(10.0f, 10.0f);
	s->body_bottom->user = s;

	s->goright = false;

	physics_body_body_callback(s->body_top, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(s->body_bottom, collide_with_player, BODY_COLLISION_PLAYER);

	physics_body_body_callback(s->body_top, collide_with_projectile, BODY_COLLISION_PROJECTILES);
	physics_body_body_callback(s->body_bottom, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return s;
}
