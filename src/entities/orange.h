#ifndef ORANGE_H
#define ORANGE_H

// NOTE(legalprisoner8140):
// Orange Crystal

#include "./entity.h"
#include "map/world.h"

struct orange {
	ENTITY_HEADER

	struct physics_body *body;
	float wave;
	int hp;
	int tick;
	int y;
};

struct orange *orange_create(struct world *world, vec2_t global_pos);

#endif
