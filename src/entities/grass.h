#ifndef GRASS_H
#define GRASS_H

#include "./entity.h"
#include "map/world.h"
#include "physics/physics.h"

struct grass {
	ENTITY_HEADER

	vec2i_t location_pos;
	vec2i_t obstacle_pos;

	uint32_t ticks;
};

struct grass *grass_create(struct world *world, vec2i_t location_pos,
                           vec2i_t obstacle_pos);

#endif
