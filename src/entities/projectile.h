#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "./entity.h"
#include "map/world.h"
#include "physics/physics.h"

enum projectile_target {
	PROJECTILE_TARGET_ENEMIES,
	PROJECTILE_TARGET_PLAYER,
};

struct projectile {
	ENTITY_HEADER

	struct physics_body *body;
	struct surface *sprite;

	/// The lifetime of the projectile. Defaults to 80 ticks.
	unsigned lifetime;
	unsigned age;

	enum projectile_target target;
};

struct projectile *projectile_create(struct world *world, vec2_t position,
                                     vec2_t velocity, vec2_t size,
                                     enum projectile_target target, struct surface *sprite);

void projectile_destroy(struct projectile *p);

#endif
