#include "spinning_ball.h"

#include <math.h>

#include "assets.h"
#include "common/graphics/surface.h"
#include "common/math/util.h"
#include "common/math/vector.h"
#include "entities/player.h"
#include "map/obstacle_data.h"
#include "map/world.h"
#include "physics/collision_groups.h"
#include "physics/physics.h"

static vec2_t
get_shift(vec2i_t global_pos, uint32_t ticks, float amplitude, bool is_ball)
{
	vec2_t shift;
	shift.x = global_pos.x + sinf(ticks * 0.07f) * amplitude;
	shift.y = global_pos.y + cosf(ticks * 0.07f) * amplitude;
	if (is_ball) {
		shift.x -= 5;
		shift.y -= 5;
	}
	return shift;
}

static void
render(struct spinning_ball *ball, struct surface *dest, float step)
{
	struct location *loc = world_get_location(ball->world, ball->location_pos);
	if (!loc)
		return;

	vec2_t ball_pos;
	ball_pos.x = lerpf(ball->ball_pos_prev.x, ball->ball_pos.x, step);
	ball_pos.y = lerpf(ball->ball_pos_prev.y, ball->ball_pos.y, step);

	// Render X-es
	for (uint8_t i = 0; i < 2; i++) {
		surface_blit(assets_get_obstacle(ID_FLYING_X), NULL, dest,
		             world_get_visual_pos(ball->world, ball->x_positions[i]));
	}

	// Render ball
	surface_blit(assets_get_obstacle(ID_SPINNING_BALL), NULL, dest,
	             world_get_visual_pos(ball->world, ball_pos));
}

static void
collide_with_player(struct physics_body *body_ball, struct physics_body *body_player)
{
	(void)body_ball;
	struct player *player = body_player->user;
	player_kill(player);
}

static void
update(struct spinning_ball *ball)
{
	ball->ticks++;

	vec2i_t global_pos = world_get_global_posi(ball->location_pos, ball->obstacle_pos);

	ball->ball_pos_prev = ball->ball_pos;
	ball->ball_pos = get_shift(global_pos, ball->ticks, 20.0f, true);
	ball->x_positions[0] = get_shift(global_pos, ball->ticks, 3.0f, false);
	ball->x_positions[1] = get_shift(global_pos, ball->ticks, 9.0f, false);

	ball->body->position = vec2(ball->ball_pos.x + 10.0f, ball->ball_pos.y + 10.0f);
}

static void
clean(struct spinning_ball *ball)
{
	ball->body->remove = true;
}

struct spinning_ball *
spinning_ball_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct spinning_ball *ball = ecalloc(1, sizeof(*ball));

	entity_init(ball, world, render, update, clean);

	ball->location_pos = location_pos;
	ball->obstacle_pos = obstacle_pos;

	ball->body = physics_body_create(world->space);
	ball->body->size = vec2(10.0f, 10.0f);
	ball->body->user = ball;

	physics_body_body_callback(ball->body, collide_with_player, BODY_COLLISION_PLAYER);

	return ball;
}
