#include "./savepoint.h"

#include <math.h>

#include "./player.h"
#include "./util.h"
#include "game_save.h"
#include "map/obstacle_data.h"
#include "physics/collision_groups.h"
#include "sound_system.h"

static void
render(struct savepoint *sp, struct surface *s, float step)
{
	struct game *game = sp->world->game;
	entity_util_draw_sprite(game, s, sp->body, assets_get_obstacle(ID_SAVEPOINT), vec2i(0, 0),
	                        step);

	if (sp->collected) {
		// Center
		vec2i_t offset;
		offset.x = -27;
		offset.y = -27;
		struct surface *surf = g_assets.savepoint_shine;
		surface_set_blending_mode(surf, SURFACE_BLENDING_MODE_ADD);
		surface_set_alpha(surf, 127 + sinf(sp->ticks_since_collected / 20.0f) * 127.0f);
		entity_util_draw_sprite(game, s, sp->body, surf, offset, step);
	}
}

static void
update(struct savepoint *sp)
{
	if (sp->collected)
		++sp->ticks_since_collected;

	if (sp->ticks_since_collected == 1) {
		sound_system_play(sp->world->game->sound_sys, SAMPLE_SAVEPOINT_MARK);
	}
}

static void
clean(struct savepoint *sp)
{
	sp->body->remove = true;
}

static void
collide_with_player(struct physics_body *bsp, struct physics_body *bplayer)
{
	struct player *p = (struct player *)bplayer->user;
	struct savepoint *sp = (struct savepoint *)bsp->user;
	struct game *game = p->world->game;

	if (sp->collected) // Do not save twice
		return;

	p->respawn_point = bsp->position;
	sp->collected = true;

	if (game->gamesave == NULL)
		game->gamesave = game_save_create_empty();

	game->gamesave->checkpoint_position =
	    (vec2i_t){(int)p->respawn_point.x, (int)p->respawn_point.y};
	game->gamesave->max_bullets = 69; // TODO: Add difficulties

	char *path = game_save_get_path(GAME_SAVE_FILE_NAME);
	game_save_write_file(game->gamesave, path);
	free(path);
}

struct savepoint *
savepoint_create(struct world *world, vec2_t position)
{
	struct savepoint *s = ecalloc(1, sizeof(*s));

	entity_init(s, world, render, update, clean);

	s->body = physics_body_create(world->space);
	s->body->position = position;
	s->body->size = vec2(10, 10);
	s->body->user = s;

	physics_body_body_callback(s->body, collide_with_player, BODY_COLLISION_PLAYER);

	return s;
}
