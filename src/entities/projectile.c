#include "projectile.h"

#include <stdlib.h>

#include "./util.h"
#include "particles/smoke.h"
#include "physics/collision_groups.h"

static void
render(struct projectile *p, struct surface *s, float step)
{
	entity_util_draw_sprite(p->world->game, s, p->body, p->sprite, vec2i(0, 0), step);
}

static void
update(struct projectile *p)
{
	++p->age;
	p->remove = p->remove || p->age >= p->lifetime;

	// Generate smoke particles
	for (int i = 0; i < 4; i++) {
		vec2_t vel;
		vel.x = (rand() % 1000 - 500) * 0.001f;
		vel.y = (rand() % 1000 - 500) * 0.001f;
		uint8_t brightness = rand() % 256;
		world_spawn_particle(
		    p->world,
		    particle_smoke_create(p->world, add2v(p->body->position, vec2(2, 2)), vel,
		                          brightness, brightness, brightness, 5 + rand() % 20));
	}
}

static void
clean(struct projectile *p)
{
	p->body->remove = true;
}

struct projectile *
projectile_create(struct world *world, vec2_t position, vec2_t velocity,
                  vec2_t size, enum projectile_target target, struct surface *sprite)
{
	struct projectile *p = ecalloc(1, sizeof(*p));

	entity_init(p, world, render, update, clean);

	p->body = physics_body_create(world->space);
	p->body->position = position;
	p->body->velocity = velocity;
	p->body->size = size;
	p->body->obstacle_response_mask = OBSTACLE_COLLISION_NONE;
	p->body->group = GROUP_PROJECTILE;
	p->body->collision_callback_mask = BODY_COLLISION_PROJECTILES;
	p->body->user = p;
	p->sprite = sprite;

	p->lifetime = 80;
	p->age = 0;
	p->target = target;

	return p;
}

void 
projectile_destroy(struct projectile *p)
{
	assert(p != NULL);

	p->age = p->lifetime;
}
