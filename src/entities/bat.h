#ifndef BAT_H
#define BAT_H

#include "./entity.h"
#include "map/world.h"

struct bat {
	ENTITY_HEADER

	struct physics_body *body;
	int hp;
	int tick;
	vec2_t vel;
};

struct bat *bat_create(struct world *world, vec2_t global_pos);

#endif
