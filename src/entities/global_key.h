#ifndef GLOBAL_KEY_H
#define GLOBAL_KEY_H

#include <stdint.h>

#include "./entity.h"
#include "common/math/vector.h"
#include "map/world.h"
#include "physics/physics.h"

struct global_key {
	ENTITY_HEADER

	struct physics_body *body;
	uint8_t number;
};

struct global_key *global_key_create(struct world *world, vec2_t position, uint8_t number);

#endif
