#include "slime.h"

#include "common/util.h"
#include "map/obstacle_data.h"
#include "physics/collision_groups.h"
#include "player.h"
#include "projectile.h"

static void
clean(struct slime *s)
{
	s->body->remove = true;
}

static void
render(struct slime *s, struct surface *dest, float step)
{
	// TODO: animations
	// NOTE:
	// The animation in the original is confusing, I would have preferred some other animation.

	struct surface *sprite = assets_get_obstacle(ID_SLIME);
	entity_util_draw_sprite(s->world->game, dest, s->body, sprite, vec2i(0, 0), step);
}

static void
update(struct slime *s)
{
	if (s->right) {
		s->body->velocity.x = 0.3f;
	} else {
		s->body->velocity.x = -0.3f;
	}

	s->body->velocity.y = 1.0f;

	if (physics_body_collision(s->body, WALL_LEFT, IS_COLLIDING)) {
		s->right = false;
	} else if (physics_body_collision(s->body, WALL_RIGHT, IS_COLLIDING)) {
		s->right = true;
	}
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;
	struct player *p = (struct player *)bc->user;
	player_kill(p);
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct slime *s = (struct slime *)bp->user;
	struct projectile *p = (struct projectile *)bc->user;

	if (p->target != PROJECTILE_TARGET_ENEMIES)
		return;

	projectile_destroy(p);
	s->hp--;

	if (s->hp <= 0) {
		s->remove = true;
	}
}

struct slime *
slime_create(struct world *world, vec2_t global_pos)
{
	struct slime *s = ecalloc(1, sizeof(*s));

	entity_init(s, world, render, update, clean);

	s->body = physics_body_create(world->space);
	s->body->position = global_pos;
	s->body->size = vec2(10, 10);
	s->body->user = s;

	s->right = false;
	s->hp = 3;

	physics_body_body_callback(s->body, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(s->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return s;
}
