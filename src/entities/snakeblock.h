#ifndef SNAKEBLOCK_H
#define SNAKEBLOCK_H

#include <stdint.h>

#include "./entity.h"
#include "map/location.h"
#include "map/world.h"

struct sb_generator {
	ENTITY_HEADER

	vec2i_t location_pos;
	vec2i_t obstacle_pos;
	uint32_t ticks;
};

struct sb_generator *sb_generator_create(struct world *world, vec2i_t location_pos,
                                         vec2i_t obstacle_pos);

struct snakeblock {
	ENTITY_HEADER

	struct physics_body *body;
	int direction;
	uint32_t ticks;
	vec2i_t location_pos;
	vec2i_t obstacle_pos;
};

struct snakeblock *snakeblock_create(struct world *world, vec2i_t location_pos,
                                     vec2i_t obstacle_pos);

#endif
