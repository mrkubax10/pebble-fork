#include "push.h"

#include <math.h>

#include "backends/audio/audio.h"
#include "common/graphics/surface.h"
#include "common/util.h"
#include "entities/entity.h"
#include "map/location.h"
#include "map/world.h"
#include "particles/push_anim.h"
#include "physics/collision_groups.h"
#include "projectile.h"
#include "sound_system.h"

static void
clean(struct push *p)
{
	p->body->remove = true;
}

static void
update_flags(struct push *p)
{
	struct location *loc = world_get_location(p->world, p->location_pos);
	if (!loc)
		return;

	struct obstacle *obstacle = location_get_obstacle(loc, p->obstacle_pos);
	if (!obstacle)
		return;

	if (p->cooldown) {
		obstacle->flags = FLAGS_IS_SOLID | FLAGS_IS_INVISIBLE;
		p->cooldown--;
	} else {
		obstacle->flags = FLAGS_IS_SOLID | FLAGS_CASTS_SHADOW;
	}
}

static void
update(struct push *p)
{
	update_flags(p);
}

static void
move_obstacle(struct physics_body *bp, struct physics_body *bc)
{
	struct push *push = (struct push *)bp->user;

	if (push->cooldown)
		return;

	struct location *loc = world_get_location(push->world, push->location_pos);
	if (!loc)
		return;

	int offset_x = 0;
	int offset_y = 0;

	vec2_t pushpos;
	pushpos.x = bp->position.x + bp->size.x / 2.0f;
	pushpos.y = bp->position.y + bp->size.y / 2.0f;

	vec2_t otherpos;
	otherpos.x = bc->position.x + bc->size.x / 2.0f;
	otherpos.y = bc->position.y + bc->size.y / 2.0f;

	float x = otherpos.x - pushpos.x;
	float y = pushpos.y - otherpos.y;

	const float margin = 11.5f;

	// https://www.desmos.com/calculator/
	// type:
	//   x+y<0
	//   x-y<0
	// and you will know what I mean.

	if (push->horizontal && push->vertical) {
		// Left side
		if (x < -y && x < y) {
			offset_x++;
		}

		// Right side
		if (x > -y && x > y) {
			offset_x--;
		}

		// Top side
		if (x > -y && x < y) {
			offset_y++;
		}

		// Bottom side
		if (x < -y && x > y) {
			offset_y--;
		}
	} else if (push->horizontal) {
		// Left side
		if (otherpos.x < pushpos.x && fabsf(otherpos.y - pushpos.y) < margin) {
			offset_x++;
		}
		// Right side
		if (otherpos.x > pushpos.x && fabsf(otherpos.y - pushpos.y) < margin) {
			offset_x--;
		}
	} else if (push->vertical) {
		// Top side
		if (otherpos.y < pushpos.y && fabsf(otherpos.x - pushpos.x) < margin) {
			offset_y++;
		}
		// Bottom side
		if (otherpos.y > pushpos.y && fabsf(otherpos.x - pushpos.x) < margin) {
			offset_y--;
		}
	}

	if (!(offset_x || offset_y))
		return; // Not moved

	int cur_x = push->obstacle_pos.x;
	int cur_y = push->obstacle_pos.y;

	bool move = false;

	uint8_t move_positions_count = 0;
	vec2i_t move_positions[32];

	while (true) {
		struct obstacle *obstacle = location_get_obstacle(loc, vec2i(cur_x, cur_y));
		if (!obstacle)
			break;

		if (obstacle->id == 0) {
			move = true;
			break;
		}

		if (obstacle->id == ID_PUSH_H || obstacle->id == ID_PUSH_HV ||
		    obstacle->id == ID_PUSH_V) {

			if (move_positions_count < 32)
				move_positions[move_positions_count++] = vec2i(cur_x, cur_y);

			cur_x += offset_x;
			cur_y += offset_y;
			continue;
		}

		break;
	}

	if (move) {
		location_move_obstacle(loc, push->obstacle_pos, vec2i(cur_x, cur_y));
		push->body->position.x = (loc->pos.x * LOCATION_WIDTH + cur_x) * OBSTACLE_SCALE - 1.0f;
		push->body->position.y = (loc->pos.y * LOCATION_HEIGHT + cur_y) * OBSTACLE_SCALE - 1.0f;
		push->obstacle_pos.x = cur_x;
		push->obstacle_pos.y = cur_y;

		// Spawn particles
		for (uint8_t i = 0; i < move_positions_count; i++) {
			sound_system_play(push->world->game->sound_sys, SAMPLE_PUSH);
			vec2i_t pos = move_positions[i];
			vec2_t cur_pos, target_pos;
			cur_pos.x = (loc->pos.x * LOCATION_WIDTH + pos.x) * OBSTACLE_SCALE;
			cur_pos.y = (loc->pos.y * LOCATION_HEIGHT + pos.y) * OBSTACLE_SCALE;
			target_pos.x = cur_pos.x + offset_x * OBSTACLE_SCALE;
			target_pos.y = cur_pos.y + offset_y * OBSTACLE_SCALE;

			world_spawn_particle(push->world,
			                     particle_push_anim_create(push->world, cur_pos, target_pos));
		}

		push->cooldown = 8;
		update_flags(push);
	}
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	move_obstacle(bp, bc);
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct projectile *proj = (struct projectile *)bc->user;

	projectile_destroy(proj);

	move_obstacle(bp, bc);
}

struct push *
push_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos,
            bool allow_horizontal, bool allow_vertical)
{
	struct push *p = ecalloc(1, sizeof(*p));

	entity_init(p, world, NULL, update, clean);

	vec2_t global_pos = world_get_global_pos(location_pos, obstacle_pos);

	p->body = physics_body_create(world->space);
	p->body->position = sub2f(global_pos, 1);
	p->body->size = vec2(12, 12);
	p->body->user = p;

	p->obstacle_pos = obstacle_pos;
	p->location_pos = location_pos;

	p->horizontal = allow_horizontal;
	p->vertical = allow_vertical;

	physics_body_body_callback(p->body, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(p->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return p;
}
