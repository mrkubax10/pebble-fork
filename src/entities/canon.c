#include "canon.h"

#include <math.h>

#include "assets.h"
#include "entities/player.h"
#include "map/obstacle_data.h"
#include "projectile.h"

static void
bullet_update(struct projectile *p)
{
	++p->age;
	p->remove = p->remove || p->age >= p->lifetime;
}

static void
targetted_bullet_update(struct projectile *p)
{
	const vec2i_t visual_pos = world_get_visual_pos(p->world, p->body->position);
	p->remove = p->remove || visual_pos.x < -OBSTACLE_SCALE || visual_pos.x > LOCATION_WIDTH * OBSTACLE_SCALE ||
		visual_pos.y < -OBSTACLE_SCALE || visual_pos.y > LOCATION_HEIGHT * OBSTACLE_SCALE;
}

static vec2_t
get_vector_towards_position(struct canon *c, vec2_t target)
{
	const vec2_t position_diff = sub2v(target, c->body_center);
	const float vector_length = sqrtf(powf(position_diff.x, 2.0f) + powf(position_diff.y, 2.0f));
	return div2f(position_diff, vector_length);
}

static vec2_t
get_vector_towards_player(struct canon *c)
{
	const struct player *p = world_get_closest_player(c->world, c->body_center);
	if(!p)
		return vec2(0.0f, 0.0f);

	const vec2_t player_body_center = {
		p->body->position.x + p->body->size.x / 2.0f,
		p->body->position.y + p->body->size.y / 2.0f,
	};
	return get_vector_towards_position(c, player_body_center);
}

static vec2_t
find_target_position(struct canon *c)
{
	struct location *loc = world_get_location(c->world, c->world->current_loc_pos);
	for (int x = 0; x < LOCATION_WIDTH; x++) {
		for (int y = 0; y < LOCATION_HEIGHT; y++) {
			struct obstacle *ob = location_get_obstacle(loc, vec2i(x, y));
			if (!ob)
				continue;

			struct obstacle *ob_next = location_get_obstacle(loc, vec2i(x + 1, y));
			if (!ob_next)
				continue;

			if (ob->id == ID_WARDROBE && ob_next->id == ID_BED) {
				const vec2_t global_pos = world_get_global_pos(c->world->current_loc_pos, vec2i(x, y));
				return add2v(global_pos, div2f(vec2(OBSTACLE_SCALE, OBSTACLE_SCALE), 2.0f));
			}
		}
	}

	return c->body_center;
}

static void
clean(struct canon *c)
{
	c->body->remove = true;
}

static void
render(struct canon *c, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_CANON_LEFT + c->type);

	vec2i_t offset;
	switch (c->type) {
	case CANON_LEFT:
		offset = vec2i(c->offset, 0.0f);
		break;
	case CANON_UP:
		offset = vec2i(0.0f, c->offset);
		break;
	case CANON_RIGHT:
		offset = vec2i(-c->offset, 0.0f);
		break;
	case CANON_DOWN:
		offset = vec2i(0.0f, -c->offset);
		break;
	}

	entity_util_draw_sprite(c->world->game, s, c->body, sprite, offset, step);
}

static void
update(struct canon *c)
{
	if (c->type == CANON_TARGET_POSITION && !c->position_target_found) {
		c->bullet_vel = get_vector_towards_position(c, find_target_position(c));
		c->position_target_found = true;
	}

	c->tick++;

	c->offset *= 0.9f;

	if (c->offset < 1.0f)
		c->offset = 0.0f;

	if (c->tick % 60 == 0) {
		const vec2_t bullet_vel = c->type == CANON_TARGET_PLAYER ? get_vector_towards_player(c) : c->bullet_vel;
		struct projectile *p = 
			projectile_create(c->world, c->body->position, bullet_vel,
			                  vec2(10.0f, 10.0f), PROJECTILE_TARGET_PLAYER, 
							  g_assets.projectiles[6]);

		p->update_impl = (entity_update_fn)(c->type < CANON_TARGET_POSITION ? bullet_update : targetted_bullet_update);

		world_spawn_entity(c->world, p);

		c->offset = 10.0f;
	}
}

struct canon *
canon_create(struct world *world, vec2_t global_pos, int type)
{
	struct canon *c = ecalloc(1, sizeof(*c));

	entity_init(c, world, render, update, clean);

	c->body = physics_body_create(world->space);
	c->body->position = global_pos;
	c->body->size = vec2(10, 10);
	c->body->user = c;

	c->type = type;
	c->tick = 0.0f;
	c->offset = 0.0f;
	c->position_target_found = false;

	switch (type) {
	case CANON_LEFT:
		c->bullet_vel = vec2(-3.5f, 0.0f);
		break;
	case CANON_UP:
		c->bullet_vel = vec2(0.0f, -3.5f);
		break;
	case CANON_RIGHT:
		c->bullet_vel = vec2(3.5f, 0.0f);
		break;
	case CANON_DOWN:
		c->bullet_vel = vec2(0.0f, 3.5f);
		break;
	case CANON_TARGET_POSITION:
	case CANON_TARGET_PLAYER:
		c->body_center = add2v(c->body->position, div2f(c->body->size, 2.0f));
		break;
	}

	return c;
}
