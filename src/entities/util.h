#ifndef ENTITIES_UTIL_H
#define ENTITIES_UTIL_H

#include "common/math/rect.h"
#include "game.h"
#include "physics/physics.h"

void entity_util_draw_sprite(struct game *game, struct surface *dest, struct physics_body *body,
                             struct surface *sprite, const vec2i_t offset, float step);

void entity_draw_hp_bar(int hp, struct surface *dest);

#endif
