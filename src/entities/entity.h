/// @addtogroup entity Entity
/// @{

#ifndef ENTITY_H
#define ENTITY_H

#include "common/graphics/surface.h"
#include "common/math/vector.h"
#include "util.h"

// Inheritance here works by the way of adding special fields that we can
// simply cast into a struct. The field order is important: these fields *must*
// be the first in the struct that "inherits" from struct entity. For example:
//
// ```c
// struct enemy {
//   ENTITY_HEADER
//
//   int kind;
// }
// ```
//
// This exploits the fact that we know the byte layout of a struct.
// For instance, for the above entity:
//
// ```
// struct enemy
//
//   ENTITY_HEADER:
//   0  - remove: 1 byte
//   4  - draw_impl: 8 bytes
//   12 - update_impl: 8 bytes
//   ... etc.
//
//	 other fields:
//   xx - kind: 4 bytes
// ```
//
// struct entity is defined as just the entity header, so we can only read from
// the fields defined here, and the other fields get truncated.

struct entity;

// To simulate dynamic dispatch (virtual methods in C++ lingo), we'll basically
// store our "virtual" functions as callbacks in the entity header.
// This is actually how vtables are implemented in C++.

typedef void (*entity_render_fn)(struct entity *, struct surface * /*target*/, float /*step*/);

typedef void (*entity_update_fn)(struct entity *);

typedef void (*entity_clean_fn)(struct entity *);

/// The entity header.
///
/// `remove` - signals the world to remove the entity after the current tick.
///
/// `remove_offscreen` - signals the world to remove the entity if it's
/// off-screen. Defaults to `true`.
#define ENTITY_HEADER             \
	struct world *world;          \
	bool remove;                  \
	bool remove_offscreen;        \
	entity_render_fn render_impl; \
	entity_update_fn update_impl; \
	entity_clean_fn clean_impl;

struct entity {
	ENTITY_HEADER
};

void entity_init__impl(struct entity *ent, struct world *world, entity_render_fn render_impl,
                       entity_update_fn update_impl, entity_clean_fn clean_impl);

void entity_render__impl(struct entity *ent, struct surface *s, float step);

void entity_update__impl(struct entity *ent);

void entity_clean__impl(struct entity *ent);

#define entity_init(e, world, render_fn, update_fn, clean_fn)                 \
	entity_init__impl((struct entity *)e, world, (entity_render_fn)render_fn, \
	                  (entity_update_fn)update_fn, (entity_clean_fn)clean_fn)

#define entity_render(e, s, t) entity_render__impl((struct entity *)e, s, t)

#define entity_update(e) entity_update__impl((struct entity *)e)

/// @brief Cleans up the entity.
/// This frees `e` and calls the clean_impl hook.
#define entity_clean(e) entity_clean__impl((struct entity *)e)

#endif

/// @}
