#include "paper_brick.h"

#include "map/location.h"
#include "map/world.h"
#include "physics/collision_groups.h"

static void
clean(struct paper_brick *p)
{
	struct location *loc = world_get_location(p->world, p->location_pos);
	if (loc)
		location_remove_obstacle(loc, p->obstacle_pos);

	p->body->remove = true;
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bc;
	
	struct paper_brick *p = (struct paper_brick *)bp->user;
	
	// TODO: particles

	p->remove = true;
}

struct paper_brick *
paper_brick_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct paper_brick *p = ecalloc(1, sizeof(*p));

	entity_init(p, world, NULL, NULL, clean);

	p->body = physics_body_create(world->space);
	p->body->position = world_get_global_pos(location_pos, obstacle_pos);
	p->body->size = vec2(10, 12);
	p->body->user = p;

	p->location_pos = location_pos;
	p->obstacle_pos = obstacle_pos;

	physics_body_body_callback(p->body, collide_with_player, BODY_COLLISION_PLAYER);

	return p;
}
