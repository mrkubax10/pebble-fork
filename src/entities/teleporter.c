#include "./teleporter.h"

#include "./util.h"
#include "assets.h"
#include "map/obstacle_data.h"
#include "physics/collision_groups.h"

static void
render(struct teleporter *t, struct surface *s, float step)
{
	entity_util_draw_sprite(t->world->game, s, t->body, assets_get_obstacle(ID_TELEPORTER),
	                        vec2i(0, 0), step);
}

static void
clean(struct teleporter *t)
{
	t->body->remove = true;
}

static void
collide_with_player(struct physics_body *btp, struct physics_body *bplayer)
{
	struct teleporter *t = (struct teleporter *)btp->user;
	if (t->dest == NULL || t->remove)
		return;

	bplayer->position = t->dest->body->position;
	bplayer->position_prev = bplayer->position;
	t->remove = true;
	t->dest->remove = true;
}

struct teleporter *
teleporter_create(struct world *world, vec2_t position)
{
	struct teleporter *t = ecalloc(1, sizeof(*t));

	entity_init(t, world, render, NULL, clean);

	t->body = physics_body_create(world->space);
	t->body->position = position;
	t->body->size = vec2(20, 20);
	t->body->user = t;

	physics_body_body_callback(t->body, collide_with_player, BODY_COLLISION_PLAYER);

	return t;
}
