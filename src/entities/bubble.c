#include "bubble.h"

#include <stdio.h>
#include <math.h>

#include "common/log.h"
#include "map/obstacle_data.h"
#include "physics/collision_groups.h"

#define OFFSET_Y 2

static void
render(struct bubble *b, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_BUBBLE);

	int random = rand() % 20 - 10;
	int offx = (round(random * 0.1) * b->fall);

	entity_util_draw_sprite(b->world->game, s, b->body_fp, sprite, vec2i(offx, OFFSET_Y), step);
}

static void
update(struct bubble *b)
{
	if (b->fall) {
		b->tick++;
		// NOTE(legalprisoner8140):
		// This `if` is important. I don't know why tho.
		// I literally stole this code from RE.
		if (b->tick != 49) {
			if (b->tick > 50) {
				b->falling_pos += 0.1f;
				b->body_fp->position.y += b->falling_pos;
				b->body_fo->position.y += b->falling_pos;
			}
		}
	}

	vec2i_t pos = world_get_pos_in_location(b->body_fo->position);
	
	if (pos.y > 180) {
		b->remove = true;
	}
}

static void
clean(struct bubble *b)
{
	struct location *loc = world_get_location(b->world, b->location_pos);
	if (!loc)
		return;

	// Remove bubble collision
	location_remove_obstacle(loc, b->obstacle_pos);

	b->body_fp->remove = true;
	b->body_fo->remove = true;
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bc;

	struct bubble *b = (struct bubble *)bp->user;

	b->fall = true;
}

static void
collide_with_obstacle(struct physics_body *bp, struct obstacle ob, vec2i_t pos)
{
	(void)ob;

	struct bubble *b = (struct bubble *)bp->user;

	// Ignore our obstacle
	int in_loc_y = pos.y % LOCATION_HEIGHT;
	if (b->obstacle_pos.y == in_loc_y)
		return;

	b->remove = true;
}

struct bubble *
bubble_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct bubble *b = ecalloc(1, sizeof(*b));

	entity_init(b, world, render, update, clean);

	b->body_fp = physics_body_create(world->space);
	b->body_fo = physics_body_create(world->space);

	vec2_t pos = world_get_global_pos(location_pos, obstacle_pos);
	pos.y -= 2;

	// We need two bodies. One for collision with player (to collide from bottom
	// and from top), and to collide with other obstacles (in order to break bubble).

	b->body_fp->position = pos;
	b->body_fp->size = vec2(10, 14);
	b->body_fp->user = b;

	pos.y += 2;
	b->body_fo->position = pos;
	b->body_fo->size = vec2(10, 10);
	b->body_fo->user = b;

	b->fall = false;
	b->tick = 0;
	b->obstacle_pos = obstacle_pos;
	b->location_pos = location_pos;

	physics_body_body_callback(b->body_fp, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_obstacle_callback(b->body_fo, collide_with_obstacle, OBSTACLE_COLLISION_SOLID);

	return b;
}
