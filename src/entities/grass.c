#include "grass.h"

#include <math.h>
#include <stdio.h>

#include "./util.h"
#include "assets.h"
#include "common/graphics/surface.h"
#include "common/math/rect.h"
#include "map/location.h"
#include "map/obstacle_data.h"
#include "map/world.h"

static void
render(struct grass *g, struct surface *s, float step)
{
	(void)step;

	struct surface *sprite = assets_get_obstacle(ID_TALLGRASS);

	struct location *loc = world_get_location(g->world, g->location_pos);
	if (!loc)
		return;

	vec2i_t visual_pos = world_get_visual_pos2(g->world, g->location_pos, g->obstacle_pos);

	struct recti srcrect;
	srcrect.x = 0;
	srcrect.w = sprite->width;
	srcrect.h = 1;

	for (uint32_t i = 0; i < sprite->height; i++) {
		float amplitude = (1.0f - i / (float)sprite->height) * 2.0f;

		// sinus
		vec2i_t stripe_pos;
		stripe_pos.x = visual_pos.x;
		stripe_pos.y = visual_pos.y + i;
		srcrect.y = i;
		if (i > 0)
			stripe_pos.x += sinf(g->ticks / 20.0f + i * 0.5f + g->obstacle_pos.x) * amplitude;

		surface_blit(sprite, &srcrect, s, stripe_pos);
	}
}

static void
update(struct grass *g)
{
	g->ticks++;
}

struct grass *
grass_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct grass *g = ecalloc(1, sizeof(*g));

	entity_init(g, world, render, update, NULL);

	g->location_pos = location_pos;
	g->obstacle_pos = obstacle_pos;
	g->ticks = 0;

	return g;
}
