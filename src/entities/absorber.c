#include "absorber.h"

#include "physics/collision_groups.h"
#include "projectile.h"

static void
clean(struct absorber *a)
{
	a->body->remove = true;
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct projectile *proj = (struct projectile *)bc->user;

	// Absorb projectile
	projectile_destroy(proj);
}

struct absorber *
absorber_create(struct world *world, vec2_t global_pos)
{
	struct absorber *a = ecalloc(1, sizeof(*a));

	entity_init(a, world, NULL, NULL, clean);

	a->body = physics_body_create(world->space);
	a->body->position = global_pos;
	a->body->size = vec2(10, 10);
	a->body->user = a;

	physics_body_body_callback(a->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return a;
}
