#ifndef TELEPORTER_H
#define TELEPORTER_H

#include "./entity.h"
#include "map/world.h"

struct teleporter {
	ENTITY_HEADER

	struct physics_body *body;
	struct teleporter *dest;
};

struct teleporter *teleporter_create(struct world *world, vec2_t position);

#endif
