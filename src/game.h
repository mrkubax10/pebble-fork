#ifndef GAME_H
#define GAME_H

#include <stdbool.h>

#include "assets.h"
#include "backends/audio/audio.h"
#include "backends/render/render.h"
#include "common/graphics/transform.h"
#include "common/util.h"
#include "game_save.h"
#include "gamedata.h"
#include "misc/config.h"
#include "states/states.h"

// In order not to jump between code, you can set some flags already in
// game_init!
enum { GAME_NONE = 0x0, GAME_USE_GAMEDATA = 0x1 };

struct game {
	struct window *window;
	bool running;

	struct state **states;

	int flags;
	struct audio_context *audio_ctx;
	struct sound_system *sound_sys;
	struct input input;
	struct surface *screen;
	struct transform_stack tstack;
	struct game_save *gamesave;

	/// @brief Number of ticks (game_update() calls) since start of the
	/// application
	uint32_t total_ticks;
};

/// @brief Create new instance of Game
struct game *game_init(int flags);

/// @brief Destroy Game instance
void game_free(struct game **game);

/// @brief Poll events (mouse/keybaord etc)
void game_handle_events(struct game *game);

/// @brief Tick game (preferably 60 times/s)
void game_update(struct game *game);

/// @brief Re-render game into window (preferably as fast as possible - interpolation is
/// applied)
void game_draw(struct game *game, float step);

/// @returns true if game is running
bool game_running(struct game *game);

/// @brief Stop game (makes game_running return false)
void game_stop(struct game *game);

void game_change_state(struct game *game, struct state *state);
void game_push_state(struct game *game, struct state *state);

/// @param data A pointer to the data that will be transferred to the previous state (if any),
/// can be NULL
/// @returns true if state has been popped
bool game_pop_state(struct game *game, void *data);

#endif
