#include "states.h"

struct {
	int key;
	struct state *value;
} * states;

struct state *
state_create(init_fn init, clean_fn clean, pause_fn pause, resume_fn resume, update_fn update,
             draw_fn draw)
{
	struct state *state = ecalloc(1, sizeof(struct state));

	state->init = init;
	state->clean = clean;
	state->pause = pause;
	state->resume = resume;
	state->update = update;
	state->draw = draw;

	return state;
}

void
state_clean(struct state **_state)
{
	if (!_state)
		return;

	struct state *state = *_state;

	if (!state)
		return;

	free(state);

	*_state = NULL;
}
