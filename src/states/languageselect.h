#ifndef LANGUAGESELECT_STATE_H
#define LANGUAGESELECT_STATE_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assets.h"
#include "backends/render/render.h"
#include "common/math/math.h"
#include "game.h"
#include "menu.h"
#include "misc/config.h"
#include "states.h"

struct languageselect;

struct state *languageselect_create();

#endif
