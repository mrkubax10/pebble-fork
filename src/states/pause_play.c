#include "pause_play.h"

#include <math.h>

#include "backends/audio/audio.h"
#include "common/graphics/surface.h"
#include "common/graphics/text.h"
#include "joystick_binds.h"

enum pause_play_menu_element {
	PAUSE_PLAY_MENU_BACK_TO_GAME,
	PAUSE_PLAY_MENU_BACK_TO_MAINMENU,
	PAUSE_PLAY_MENU_BACK_TO_CHECKPOINT,

	PAUSE_PLAY_MENU_ELEMENT__LAST,
};

const char *pause_play_menu_element_names[PAUSE_PLAY_MENU_ELEMENT__LAST] = {
    "Return to the game", "Return to the game menu", "Return to the checkpoint"};

struct pauseplaystate {
	struct game *game;

	struct surface *blurred_background;

	struct surface *opt_texts[PAUSE_PLAY_MENU_ELEMENT__LAST];
	vec2i_t opt_positions[PAUSE_PLAY_MENU_ELEMENT__LAST];

	int current_option;
};

struct pauseplaystate *pauseplay_state = NULL;

static void
quit()
{
	if (!pauseplay_state)
		return;

	if (pauseplay_state->game)
		game_stop(pauseplay_state->game);
}

static struct surface *
blur_surface(struct surface *surface, int blur_intensity)
{
	struct surface *blurred =
	    surface_create_empty(surface->width, surface->height, surface->pixel_format);

	struct surface *blur_buffer =
	    surface_create_empty(surface->width, surface->height, surface->pixel_format);

	// Horizontal blur
	for (int y = 0; y < (int)surface->height; y++) {
		for (int x = 0; x < (int)surface->width; x++) {
			uint32_t sum = 0, sum_r = 0, sum_g = 0, sum_b = 0;
			for (int xx = x - blur_intensity; xx < x + blur_intensity; xx++) {
				if (xx >= 0 && xx < (int)surface->width) {
					uint8_t r, g, b, a;
					surface_get_pixel(surface, xx, y, &r, &g, &b, &a);
					sum_r += r;
					sum_g += g;
					sum_b += b;
					sum++;
				}
			}
			surface_set_pixel(blur_buffer, x, y, sum_r / sum, sum_g / sum, sum_b / sum, 255);
		}
	}

	// Vertical blur
	for (int x = 0; x < (int)surface->width; x++) {
		for (int y = 0; y < (int)surface->height; y++) {
			uint32_t sum = 0, sum_r = 0, sum_g = 0, sum_b = 0;
			for (int yy = y - blur_intensity; yy < y + blur_intensity; yy++) {
				if (yy >= 0 && yy < (int)surface->height) {
					uint8_t r, g, b, a;
					surface_get_pixel(blur_buffer, x, yy, &r, &g, &b, &a);
					sum_r += r;
					sum_g += g;
					sum_b += b;
					sum++;
				}
			}
			surface_set_pixel(blurred, x, y, sum_r / sum, sum_g / sum, sum_b / sum, 255);
		}
	}

	surface_clean(&blur_buffer);

	return blurred;
}

void
pauseplaystate_init(struct game *game)
{
	if (pauseplay_state)
		return;

	pauseplay_state = ecalloc(1, sizeof(*pauseplay_state));

	pauseplay_state->game = game;
	assert(pauseplay_state->game != NULL);

	// Blurred background surface
	pauseplay_state->blurred_background = blur_surface(game->screen, 2);

	// Texts

	struct text_render_settings settings = text_render_settings_defaults();
	settings.outlined = true;

	settings.color = (struct color_rgb){0, 253, 253};
	pauseplay_state->opt_texts[0] =
	    text_render(g_assets.main_font, pause_play_menu_element_names[0], &settings);

	settings.color = (struct color_rgb){253, 253, 0};
	pauseplay_state->opt_texts[1] =
	    text_render(g_assets.main_font, pause_play_menu_element_names[1], &settings);

	settings.color = (struct color_rgb){168, 69, 0};
	pauseplay_state->opt_texts[2] =
	    text_render(g_assets.main_font, pause_play_menu_element_names[2], &settings);

	for (int i = 0; i < PAUSE_PLAY_MENU_ELEMENT__LAST; ++i) {
		pauseplay_state->opt_positions[i].x = 20 + 25;
		pauseplay_state->opt_positions[i].y = (20 + 8 + (20 * i)) - 1;
	}

	pauseplay_state->current_option = 0;

	joystick_set_binds_menu(&game->input);

	// Register event callbacks
	input_on_quit(&game->input, quit);
}

void
pauseplaystate_clean()
{
	if (!pauseplay_state)
		return;

	surface_clean(&pauseplay_state->blurred_background);

	for (int i = 0; i < PAUSE_PLAY_MENU_ELEMENT__LAST; ++i) {
		surface_clean(&pauseplay_state->opt_texts[i]);
	}

	free(pauseplay_state);
	pauseplay_state = NULL;
}

void
pauseplaystate_pause()
{
}

void
pauseplaystate_resume()
{
	joystick_set_binds_menu(&pauseplay_state->game->input);
	surface_blit(pauseplay_state->game->screen, NULL, pauseplay_state->blurred_background,
	             (vec2i_t){0, 0});
}

void
pauseplaystate_update(struct game *game)
{
	assert(game != NULL);

	if (input_key(&game->input, KEY_UP, STATE_JUST_PRESSED)) {
		pauseplay_state->current_option--;
	}

	if (input_key(&game->input, KEY_DOWN, STATE_JUST_PRESSED)) {
		pauseplay_state->current_option++;
	}

	pauseplay_state->current_option =
	    clampi(pauseplay_state->current_option, 0, PAUSE_PLAY_MENU_ELEMENT__LAST - 1);

	if (input_key(&game->input, KEY_ENTER, STATE_JUST_PRESSED)) {
		switch (pauseplay_state->current_option) {
		case PAUSE_PLAY_MENU_BACK_TO_GAME:
			game_pop_state(game, NULL);
			break;
		case PAUSE_PLAY_MENU_BACK_TO_MAINMENU:
			game_pop_state(game, NULL);
			game_pop_state(game, NULL);
			break;
		case PAUSE_PLAY_MENU_BACK_TO_CHECKPOINT: {
			struct pauseplaystate_data data;
			data.back_to_checkpoint = true;
			game_pop_state(game, &data);
			break;
		}
		default:
			assert(false);
		}
	}
}

void
pauseplaystate_draw(struct game *game, float step)
{
	(void)step;

	assert(game != NULL);

	// Draw blurred background first
	surface_blit(pauseplay_state->blurred_background, NULL, game->screen, (vec2i_t){0, 0});

	// Then this black box with transparency
	surface_fill_blend(game->screen, &(struct recti){20, 20, 200, 65}, 0, 0, 0, 100);

	// Texts
	for (int i = 0; i < PAUSE_PLAY_MENU_ELEMENT__LAST; ++i) {
		surface_blit(pauseplay_state->opt_texts[i], NULL, game->screen,
		             pauseplay_state->opt_positions[i]);
	}

	// Arrow

	vec2i_t arrow_pos = pauseplay_state->opt_positions[pauseplay_state->current_option];
	arrow_pos.x -= 22;
	arrow_pos.y -= 4;

	surface_blit(g_assets.pause_play_menu_arrow, NULL, game->screen, arrow_pos);
}

struct state *
pauseplaystate_create()
{
	return state_create((init_fn)pauseplaystate_init, (clean_fn)pauseplaystate_clean,
	                    (pause_fn)pauseplaystate_pause, (resume_fn)pauseplaystate_resume,
	                    (update_fn)pauseplaystate_update, (draw_fn)pauseplaystate_draw);
}
