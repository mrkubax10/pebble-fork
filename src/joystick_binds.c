#include "joystick_binds.h"

void
joystick_set_binds_menu(struct input *input)
{
	input->joystick_binds.dpad_up = KEY_UP;
	input->joystick_binds.dpad_down = KEY_DOWN;
	input->joystick_binds.dpad_left = KEY_LEFT;
	input->joystick_binds.dpad_right = KEY_RIGHT;

	input->joystick_binds.X = KEY_ENTER;
	input->joystick_binds.O = KEY_ESC;
}

void
joystick_set_binds_game(struct input *input)
{
	input->joystick_binds.dpad_up = KEY_UP;
	input->joystick_binds.dpad_down = KEY_DOWN;
	input->joystick_binds.dpad_left = KEY_LEFT;
	input->joystick_binds.dpad_right = KEY_RIGHT;

	input->joystick_binds.X = KEY_JUMP;
	input->joystick_binds.O = KEY_SHOOT;
	input->joystick_binds.LB = KEY_SHOOT;
	input->joystick_binds.RB = KEY_SHOOT;

	input->joystick_binds.start = KEY_ESC;
	input->joystick_binds.select = KEY_SHOOT;
}
