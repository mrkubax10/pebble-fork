/// @addtogroup location Location
/// @{

#ifndef LOCATION_H
#define LOCATION_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "common/math/vector.h"

#define OBSTACLE_SCALE 10

#define LOCATION_WIDTH  32
#define LOCATION_HEIGHT 18
#define MAX_OBSTACLES   LOCATION_WIDTH *LOCATION_HEIGHT
#define HEADER_SIZE     MAX_OBSTACLES + 1

enum obstacle_flags {
	FLAGS_NONE = 0x0,
	FLAGS_IS_SOLID = 0x1,
	FLAGS_CASTS_SHADOW = 0x2,
	FLAGS_IS_INVISIBLE = 0x4,
};

struct obstacle {
	uint16_t id;
	uint8_t flags;
	uint8_t data;
};

struct location_header {
	uint8_t flags; // always zero
	uint8_t background;
	uint8_t music;
	uint8_t empty; // always zero; for future use
};

struct location {
	vec2i_t pos;
	struct location_header header;
	struct obstacle obstacles[MAX_OBSTACLES];
	uint32_t background_id;
	uint8_t *shadow_alpha_mask;    // For shadow rendering
	uint16_t offscreen_time_ticks; // Used for location cleanup in-game
};

/// @brief Creates an empty obstacle.
///
/// An example use is removing an obstacle from a location. And since an obstacle
/// can have more fields, this is a easier way than doing it manually.
///
/// Example:
/// @code{.c}
/// location_set_obstacle(loc, vec2i(0, 0), obstacle_null());
/// @endcode
///
/// @returns Empty obstacle
struct obstacle obstacle_null(void);

/// @brief Creates an empty location.
/// @returns Pointer to a location
struct location *location_create(void);

/// @brief Copies data from one location to another.
/// @param from Pointer to location from which the data will be copied, must not be NULL
/// @param to Pointer to location to which the data will be copied, must not be NULL
void location_copy_data(struct location *from, struct location *to);

/// @brief Frees location.
/// @param location Pointer to pointer to location, to change the pointer to NULL and free the
/// location
void location_free(struct location **location);

/// @brief Loads a location from array.
///
/// Example:
/// @code{.c}
/// const uint8_t file[] = {
/// 	0x00, 0x01, 0x00, 0x00,
/// 	0x00, 0x01, 0x00, 0x00,
/// };
///
/// struct location *loc = location_create();
/// location_load_array(loc, file, sizeof(file) / sizeof(*file));
///
/// location_free(loc);
/// @endcode
///
/// @param file Array with data to read
/// @param size Size of the file
void location_load_array(struct location *location, const uint8_t file[], size_t size);

/// @brief Loads location from file.
/// @param path Path to a location file
void location_load(struct location *location, const char *path);

/// @brief Saves location to file.
/// @param path Path where to save file
void location_save(struct location *location, const char *path);

/// @brief Places obstacle in a location.
/// @param ob Obstacle to place, uses all fields
/// @return 0 on success, -1 on invalid position
int location_set_obstacle(struct location *location, vec2i_t obstacle_pos, struct obstacle ob);

/// @brief Removes obstacle in a location.
/// @return 0 on success, -1 on invalid position
int location_remove_obstacle(struct location *location, vec2i_t obstacle_pos);

/// @brief Returns obstacle in a location from position.
/// @param obstacle_pos Obstacle position
/// @return Obstacle pointer on success, NULL on invalid position
struct obstacle *location_get_obstacle(struct location *location, vec2i_t obstacle_pos);

/// @brief Moves obstacle from old position to new position.
/// @param old_x Old x position
/// @param old_y Old y position
/// @param new_x New x position
/// @param new_y New y position
/// @return 0 on success, -1 on invalid position
/// @note This function overrides obstacles
int location_move_obstacle(struct location *location, vec2i_t old_pos, vec2i_t new_pos);

/// @brief Sets location's background
/// Same as location->background_id = background_id.
/// @param bg Background id
void location_set_background(struct location *location, uint32_t background_id);

#ifndef LOCATION_NO_RENDER

struct surface;

/// @returns true if rendered
bool location_render_background(struct location *location, struct surface *target, int shiftX,
                                int shiftY, uint8_t alpha);

/// @brief Render location
void location_render(struct location *location, struct surface *target, int shiftX, int shiftY);

/// @brief Render postprocess of location (e.g. static shadows)
/// Automatically generates shadow mask
void location_render_postprocess(struct location *location, struct surface *surface, int shiftX,
                                 int shiftY);

/// @brief Remove location shadow mask array (320x180 pixels)
void location_visual_cleanup(struct location *location);
#endif

#endif

/// @}
