#include "location.h"

#include <stdint.h>
#include <string.h>

#include "assets.h"
#include "backends/audio/audio.h"
#include "common/graphics/surface.h"
#include "common/log.h"
#include "common/serialization.h"
#include "common/util.h"
#include "map/world.h"
#include "misc/config.h"
#include "obstacle_data.h"

#define OBSTACLE_POS(x, y) (y) * LOCATION_WIDTH + (x)

#define IS_VALID_POS(x, y)                             \
	do {                                               \
		if (x > LOCATION_WIDTH || y > LOCATION_HEIGHT) \
			return -1;                                 \
	} while (0)

struct obstacle
obstacle_null(void)
{
	struct obstacle ob;
	memset(&ob, 0, sizeof(ob));

	return ob;
}

struct location *
location_create(void)
{
	struct location *loc = ecalloc(1, sizeof(struct location));
	loc->pos.x = INT32_MIN;
	loc->pos.y = INT32_MIN;
	return loc;
}

void
location_copy_data(struct location *from, struct location *to)
{
	assert(from != NULL);
	assert(to != NULL);

	memcpy(to->obstacles, from->obstacles, sizeof(to->obstacles));
	memcpy(&to->header, &from->header, sizeof(struct location_header));
	to->pos.x = from->pos.x;
	to->pos.y = from->pos.y;
	to->background_id = from->background_id;
}

void
location_free(struct location **_location)
{
	if (!_location)
		return;

	struct location *location = *_location;
	assert(location != NULL);

#ifndef LOCATION_NO_RENDER
	location_visual_cleanup(location);
#endif
	free(location);

	*_location = NULL;
}

void
location_load_array(struct location *location, const uint8_t file[], size_t size)
{
	assert(location != NULL);

	size_t p = 0;
	struct location_header h = {0};

	h.flags = adeserialize_u8(file, &p);
	h.background = adeserialize_u8(file, &p);
	h.music = adeserialize_u8(file, &p);
	h.empty = adeserialize_u8(file, &p);

	assert(h.flags == 0);
	assert(h.empty == 0);

	location->header = h;
	location->background_id = h.background;

	for (int y = 0; y < LOCATION_HEIGHT; ++y) {
		for (int x = 0; x < LOCATION_WIDTH; ++x) {
			if (p >= size)
				goto end;

			struct obstacle obstacle;

			obstacle.id = adeserialize_u16(file, &p);
			obstacle.flags = adeserialize_u8(file, &p);
			obstacle.data = adeserialize_u8(file, &p);

			location_set_obstacle(location, vec2i(x, y), obstacle);
		}
	}
end:
	return;
}

void
location_load(struct location *location, const char *path)
{
	FILE *f = fopen(path, "rb");
	if (!f)
		return;

	fseek(f, 0, SEEK_END);
	long size = ftell(f);
	fseek(f, 0, SEEK_SET);

	uint8_t *map = (uint8_t *)emalloc(size);
	fread(map, sizeof(uint8_t), size, f);
	fclose(f);

	location_load_array(location, map, size);

	free(map);
}

void
location_save(struct location *location, const char *path)
{
	FILE *f = fopen(path, "wb");
	if (!f)
		return;

	struct location_header h = location->header;
	fserialize_u8(f, h.flags);
	fserialize_u8(f, h.background);
	fserialize_u8(f, h.music);
	fserialize_u8(f, h.empty);

	for (int i = 0; i < MAX_OBSTACLES; i++) {
		struct obstacle o = location->obstacles[i];
		fserialize_u16(f, o.id);
		fserialize_u8(f, o.flags);
		fserialize_u8(f, o.data);
	}

	fclose(f);
}

int
location_set_obstacle(struct location *location, vec2i_t obstacle_pos, struct obstacle ob)
{
	IS_VALID_POS(obstacle_pos.x, obstacle_pos.y);
	int pos = OBSTACLE_POS(obstacle_pos.x, obstacle_pos.y);
	location->obstacles[pos] = ob;
	return 0;
}

int
location_remove_obstacle(struct location *location, vec2i_t obstacle_pos)
{
	return location_set_obstacle(location, obstacle_pos, obstacle_null());
}

struct obstacle *
location_get_obstacle(struct location *location, vec2i_t obstacle_pos)
{
	assert(location != NULL);

	if (obstacle_pos.x < 0 || obstacle_pos.y < 0 || obstacle_pos.x >= LOCATION_WIDTH ||
	    obstacle_pos.y >= LOCATION_HEIGHT)
		return NULL;

	return &location->obstacles[obstacle_pos.y * LOCATION_WIDTH + obstacle_pos.x];
}

int
location_move_obstacle(struct location *location, vec2i_t old_pos, vec2i_t new_pos)
{
	IS_VALID_POS(old_pos.x, old_pos.y);
	IS_VALID_POS(new_pos.x, new_pos.y);

	int old_index = OBSTACLE_POS(old_pos.x, old_pos.y);
	int new_index = OBSTACLE_POS(new_pos.x, new_pos.y);

	location->obstacles[new_index] = location->obstacles[old_index];
	location->obstacles[old_index] = obstacle_null();

	return 0;
}

void
location_set_background(struct location *location, uint32_t background_id)
{
	location->background_id = background_id;
}

#define SHADOW_WIDTH  320
#define SHADOW_HEIGHT 180
#define SHADOW_SIZE   2

#ifndef LOCATION_NO_RENDER
static uint8_t
shadow_lerp(uint8_t n1, uint8_t n2, float t)
{
	return n1 * (1.0f - t) + n2 * (t);
}

static uint8_t
shadow_blerp(uint8_t topleft, uint8_t topright, uint8_t bottomleft, uint8_t bottomright,
             float mul_x, float mul_y)
{
	return shadow_lerp(shadow_lerp(topleft, topright, mul_x),
	                   shadow_lerp(bottomleft, bottomright, mul_x), mul_y);
}

static void
location_generate_alpha_mask(struct location *location)
{
	if (location->shadow_alpha_mask)
		return;

	// Small alpha mask
	uint8_t *alpha_mask = (uint8_t *)emalloc(LOCATION_WIDTH * LOCATION_HEIGHT);
	for (uint32_t y = 0; y < LOCATION_HEIGHT; y++) {
		for (uint32_t x = 0; x < LOCATION_WIDTH; x++) {
			uint32_t sum = 0;
			uint32_t count = 0;

			for (int yy = -SHADOW_SIZE; yy <= SHADOW_SIZE; yy++) {
				for (int xx = -SHADOW_SIZE; xx <= SHADOW_SIZE; xx++) {
					int blockX = (int)x + xx;
					int blockY = (int)y + yy;
					if (blockX < 0 || blockY < 0 || blockX >= LOCATION_WIDTH ||
					    blockY >= LOCATION_HEIGHT)
						continue;

					struct obstacle *obstacle =
					    &location->obstacles[blockY * LOCATION_WIDTH + blockX];

					if (obstacle->id == 0) {
						sum += 450;
					} else if ((obstacle->flags & FLAGS_IS_SOLID) == false) {
						sum += 170;
					}

					count++;
				}
			}

			sum /= count;

			int val = sum + 10;

			if (val < 0)
				val = 0;
			if (val > 255)
				val = 255;

			alpha_mask[y * LOCATION_WIDTH + x] = val;
		}
	}

	// Full-res alpha mask
	location->shadow_alpha_mask = (uint8_t *)emalloc(SHADOW_WIDTH * SHADOW_HEIGHT);

	// Upscale alpha mask to hi-res (with bilinear interpolation)
	uint8_t topleft, topright, bottomleft, bottomright;
	for (uint32_t y = 0; y < SHADOW_HEIGHT; y++) {
		for (uint32_t x = 0; x < SHADOW_WIDTH; x++) {
			topleft = alpha_mask[(y / 10) * LOCATION_WIDTH + x / 10];

			// OOB check
			if (x / 10 + 1 < LOCATION_WIDTH)
				topright = alpha_mask[(y / 10) * LOCATION_WIDTH + x / 10 + 1];
			else
				topright = topleft;

			if (y / 10 + 1 < LOCATION_HEIGHT)
				bottomleft = alpha_mask[(y / 10 + 1) * LOCATION_WIDTH + x / 10];
			else
				bottomleft = topleft;

			if (x / 10 + 1 < LOCATION_WIDTH && y / 10 + 1 < LOCATION_HEIGHT)
				bottomright = alpha_mask[(y / 10 + 1) * LOCATION_WIDTH + x / 10 + 1];
			else
				bottomright = bottomleft;

			float mul_x = (x - (int)(x / 10) * 10) / 10.0f;
			float mul_y = (y - (int)(y / 10) * 10) / 10.0f;

			location->shadow_alpha_mask[y * SHADOW_WIDTH + x] =
			    shadow_blerp(topleft, topright, bottomleft, bottomright, mul_x, mul_y);
		}
	}

	free(alpha_mask);
}

bool
location_render_background(struct location *location, struct surface *target, int shiftX,
                           int shiftY, uint8_t alpha)
{
	if (location->background_id == 0)
		return false; // Nothing to render

	struct surface *surf_bg = assets_get_background(location->background_id);
	if (!surf_bg)
		return false;

	for (unsigned y = 0; y < target->height; y += surf_bg->height) {
		for (unsigned x = 0; x < target->width; x += surf_bg->width) {
			if (alpha == 255) {
				surface_set_blending_mode(surf_bg, SURFACE_BLENDING_MODE_NONE);
				surface_set_alpha(surf_bg, 255);
			} else {
				surface_set_blending_mode(surf_bg, SURFACE_BLENDING_MODE_BLEND);
				surface_set_alpha(surf_bg, alpha);
			}
			surface_blit(surf_bg, NULL, target, vec2i(x + shiftX, y + shiftY));
		}
	}

	return true;
}

void
location_render(struct location *location, struct surface *target, int shiftX, int shiftY)
{
	// Render blocks
	for (int y = 0; y < LOCATION_HEIGHT; ++y) {
		for (int x = 0; x < LOCATION_WIDTH; ++x) {
			struct obstacle *ob = &location->obstacles[OBSTACLE_POS(x, y)];

			if (ob->id > 0 && !(ob->flags & FLAGS_IS_INVISIBLE)) {
				struct surface *obstacle = assets_get_obstacle(ob->id);

				surface_blit(obstacle, NULL, target,
				             vec2i(x * OBSTACLE_SCALE + shiftX, y * OBSTACLE_SCALE + shiftY));
			}
		}
	}

	if (g_config.disable_shadows)
		return;

	// Render shadow under blocks
	struct obstacle *obstacle_down, *obstacle_right, *obstacle_downright;

	for (int y = 0; y < LOCATION_HEIGHT; y++) {
		for (int x = 0; x < LOCATION_WIDTH; x++) {
			struct obstacle *center = &location->obstacles[OBSTACLE_POS(x, y)];
			if (center->flags & FLAGS_CASTS_SHADOW && !(center->flags & FLAGS_IS_INVISIBLE)) {
				// OOB check
				obstacle_down = (y + 1 < LOCATION_HEIGHT)
				                    ? (&location->obstacles[OBSTACLE_POS(x, y + 1)])
				                    : NULL;

				obstacle_right = (x + 1 < LOCATION_WIDTH)
				                     ? (&location->obstacles[OBSTACLE_POS(x + 1, y)])
				                     : NULL;

				obstacle_downright = (x + 1 < LOCATION_WIDTH && y + 1 < LOCATION_HEIGHT)
				                         ? (&location->obstacles[OBSTACLE_POS(x + 1, y + 1)])
				                         : NULL;

				bool down = obstacle_down && obstacle_down->flags & FLAGS_CASTS_SHADOW;
				bool right = obstacle_right && obstacle_right->flags & FLAGS_CASTS_SHADOW;
				bool downright =
				    obstacle_downright && obstacle_downright->flags & FLAGS_CASTS_SHADOW;

				if (down == false) {
					surface_blit(g_assets.game_shadow_bottom, NULL, target,
					             vec2i(x * 10 + shiftX, y * 10 + 10 + shiftY));
				}
				if (right == false) {
					surface_blit(g_assets.game_shadow_right, NULL, target,
					             vec2i(x * 10 + 10 + shiftX, y * 10 + shiftY));
				}
				if (downright == false && down == false && right == false) {
					surface_blit(g_assets.game_shadow_bottom_right, NULL, target,
					             vec2i(x * 10 + 10 + shiftX, y * 10 + 10 + shiftY));
				}
			}
		}
	}
}

void
location_render_postprocess(struct location *location, struct surface *surface, int shiftX,
                            int shiftY)
{
	assert(location != NULL);
	assert(surface != NULL);

	location_generate_alpha_mask(location);

	if (g_config.disable_shadows)
		return;

	if (surface->width == 320 && surface->height == 180 &&
	    surface->pixel_format == SURFACE_PIXEL_FORMAT_RGB24) {
		// Render shadow (multiply pixels)
		uint8_t *rgb = (uint8_t *)surface->pixels;
		for (uint32_t y = 0; y < surface->height; y++) {
			for (uint32_t x = 0; x < surface->width; x++) {
				int source_x = x - shiftX;
				int source_y = y - shiftY;

				if (source_x < 0 || source_y < 0 || source_x >= (int)surface->width ||
				    source_y >= (int)surface->height)
					continue;

				uint32_t index_source = source_y * surface->width + source_x;

				uint32_t index_dest = y * surface->width * 3 + x * 3;

				uint8_t brightness = location->shadow_alpha_mask[index_source];

				// Blend by brightness
				rgb[index_dest + 0] = (((uint32_t)rgb[index_dest + 0]) * brightness) / 255;
				rgb[index_dest + 1] = (((uint32_t)rgb[index_dest + 1]) * brightness) / 255;

				uint32_t blue = (((uint32_t)rgb[index_dest + 2]) * (brightness + 30)) / 255;
				if (blue > 255)
					blue = 255;
				rgb[index_dest + 2] = blue;
			}
		}
	}
}

void
location_visual_cleanup(struct location *location)
{
	assert(location != NULL);

	free(location->shadow_alpha_mask);
	location->shadow_alpha_mask = NULL;
}

#endif
