#include "sound_system.h"

#include <stdint.h>
#include <stdlib.h>

#include "assets.h"
#include "backends/audio/audio.h"
#include "common/timing.h"
#include "common/util.h"

#define CHUNK_CACHE_CAPACITY 16

struct chunk_cache_cell {
	uint32_t last_used_timestamp;
	const char *path;
	struct audio_chunk *chunk;
};

struct sound_system {
	struct audio_context *ctx;
	struct chunk_cache_cell chunk_cache[CHUNK_CACHE_CAPACITY];
};

struct sound_system *
sound_system_create(struct audio_context *ctx)
{
	if (!ctx)
		return NULL;

	struct sound_system *sys = ecalloc(1, sizeof(struct sound_system));
	sys->ctx = ctx;

	return sys;
}

void
sound_system_clean(struct sound_system **sys)
{
	if (!sys)
		return;
	if (!*sys)
		return;

	for (size_t i = 0; i < CHUNK_CACHE_CAPACITY; i++) {
		struct chunk_cache_cell *cell = &(*sys)->chunk_cache[i];
		if (cell->chunk)
			audio_chunk_clean(&cell->chunk);
	}

	free(*sys);
	*sys = NULL;
}

static struct audio_chunk *
sound_system_alloc_sample(struct sound_system *sys, const char *path)
{
	if (!sys)
		return NULL;

	// Find allocated chunk in cache
	for (size_t i = 0; i < CHUNK_CACHE_CAPACITY; i++) {
		struct chunk_cache_cell *cell = &sys->chunk_cache[i];
		if (!cell->chunk)
			continue;
		if (cell->path != path)
			continue;
		cell->last_used_timestamp = time_get_millis();
		return cell->chunk; // found it!
	}

	// Not found, load sample
	for (size_t i = 0; i < CHUNK_CACHE_CAPACITY; i++) {
		struct chunk_cache_cell *cell = &sys->chunk_cache[i];
		if (!cell->chunk) {
			char p[PATH_MAX_SIZE];
			snprintf(p, PATH_MAX_SIZE, "audio/%s", path);

			cell->chunk = assets_load_audio(sys->ctx, p);
			cell->path = path;
			cell->last_used_timestamp = time_get_millis();
			return cell->chunk;
		}
	}

	// No more room, find sample that wasn't recently used
	size_t lowest_index = 0;
	uint32_t lowest_timestamp = UINT32_MAX;
	for (size_t i = 0; i < CHUNK_CACHE_CAPACITY; i++) {
		struct chunk_cache_cell *cell = &sys->chunk_cache[i];
		if (cell->last_used_timestamp < lowest_timestamp) {
			lowest_timestamp = cell->last_used_timestamp;
			lowest_index = i;
		}
	}

	// Overwrite sample
	struct chunk_cache_cell *cell = &sys->chunk_cache[lowest_index];
	audio_chunk_clean(&cell->chunk);
	cell->chunk = assets_load_audio(sys->ctx, path);
	cell->last_used_timestamp = time_get_millis();
	cell->path = path;
	return cell->chunk;
}

/// @brief Allocate (if needed) and play sample immediately
static void
sound_alloc_play(struct sound_system *sys, const char *name, struct chunk_play_params *params)
{
	struct audio_chunk *chunk = sound_system_alloc_sample(sys, name);
	params->chunk = chunk;
	audio_chunk_play_ex(params);
}

void
sound_system_play(struct sound_system *sys, enum sample_name name)
{
	struct chunk_play_params params = chunk_play_params_defaults();
	sound_system_play_ex(sys, name, &params);
}

void
sound_system_play_ex(struct sound_system *sys, enum sample_name name,
                     struct chunk_play_params *params)
{
	switch (name) {
	case SAMPLE_MENU_SELECTION:
		sound_alloc_play(sys, "selection.opus", params);
		break;
	case SAMPLE_MENU_SELECTED:
		sound_alloc_play(sys, "selected.opus", params);
		break;
	case SAMPLE_MENU_DRUMS:
		sound_alloc_play(sys, "menu_drums.opus", params);
		break;
	case SAMPLE_SHOOT:
		sound_alloc_play(sys, "shoot.opus", params);
		break;
	case SAMPLE_JUMP:
		sound_alloc_play(sys, "jump.opus", params);
		break;
	case SAMPLE_WALK1:
		sound_alloc_play(sys, "walk1.opus", params);
		break;
	case SAMPLE_WALK2:
		sound_alloc_play(sys, "walk2.opus", params);
		break;
	case SAMPLE_WALK3:
		sound_alloc_play(sys, "walk3.opus", params);
		break;
	case SAMPLE_SAVEPOINT_MARK:
		sound_alloc_play(sys, "savepoint_mark.opus", params);
		break;
	case SAMPLE_HIT:
		sound_alloc_play(sys, "hit.opus", params);
		break;
	case SAMPLE_BREAK:
		sound_alloc_play(sys, "break.opus", params);
		break;
	case SAMPLE_BULLET_SHELL:
		sound_alloc_play(sys, "bullet_shell.opus", params);
		break;
	case SAMPLE_PUSH:
		sound_alloc_play(sys, "push.opus", params);
		break;
	case SAMPLE_SPLASH_IN:
		sound_alloc_play(sys, "splash_in.opus", params);
		break;
	case SAMPLE_SPLASH_OUT:
		sound_alloc_play(sys, "splash_out.opus", params);
		break;
	}
}
