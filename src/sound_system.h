#ifndef SOUND_SYSTEM_H
#define SOUND_SYSTEM_H

#include "backends/audio/audio.h"

struct audio_context;

/// @brief Create sound resource manager
/// @param ctx Previously opened audio context
struct sound_system *sound_system_create(struct audio_context *ctx);

/// @brief Destroy sound resource manager
void sound_system_clean(struct sound_system **sys);

enum sample_name {
	SAMPLE_MENU_SELECTION,
	SAMPLE_MENU_SELECTED,
	SAMPLE_MENU_DRUMS,

	SAMPLE_WALK1,
	SAMPLE_WALK2,
	SAMPLE_WALK3,
	SAMPLE_SHOOT,
	SAMPLE_JUMP,
	SAMPLE_SAVEPOINT_MARK,
	SAMPLE_HIT,
	SAMPLE_BREAK,
	SAMPLE_BULLET_SHELL,
	SAMPLE_PUSH,
	SAMPLE_SPLASH_IN,
	SAMPLE_SPLASH_OUT
};

/// @brief Play specific sample
void sound_system_play(struct sound_system *sys, enum sample_name name);

/// @param params: Chunk pointer is automatically set
void sound_system_play_ex(struct sound_system *sys, enum sample_name name,
                          struct chunk_play_params *params);

#endif
