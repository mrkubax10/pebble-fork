# Location

WARNING: THIS LOCATION FORMAT WILL BE OBSOLETE IN THE FUTURE(?)


Location format is bigger than original format, but it is easier to expand the game with it. Collisions, shadows, special effects or AI of the enemies are no longer dependent on the id, but on whether a given flag is assigned to it. This should help a lot mod developers.

Examples will use C language and are based on our source code.

## Representation

Location format is little endian.

The format consists of a header and maximum `LOCATION_WIDTH * LOCATION_HEIGHT` obstacles.

Header is 4 bytes in size and consists of 4 fields of 1 byte each. 

First field is `flags`. For now, it is always zero. 

Then there is the `background`, which is a background id. And `music`, which is a music id. 

Then there is the `empty`, which is always zero for now, too.

Header example:
```c
struct header {
	uint8_t flags;
	uint8_t background;
	uint8_t music;
	uint8_t empty;
};
```

Then there are obstacles. Obstacles are 4 bytes in size. Just read the file until the end. 

Obstacle consists of a 2 byte id, 1 byte flags and data, also 1 byte.

Obstacle example:
```c
struct obstacle {
	uint16_t id;
	uint8_t flags;
	uint8_t data;
};
```

## Possible flags

See `src/map/location.h`.

## Creating a location

Until the editor is created, the only way is to create levels manually, using functions from `location.h`.

### Converting original format to pebble's format

See `tools/oldnew/oldnew.c`.

## Testing a location

There is no suitable tool yet.
