#include "minunit.h"

int tests_run = 0;

#include "gamedata.h"
#include "stb/stb_ds.h"
#include <stdint.h>

/// Compared to buf_bad, this is correct.
static uint8_t buf_good[] = {
	0x01, 0x00, 0x00, 0x00, 
	0x03, 0x00, 0x00, 0x00, 
	0x04, 0x00, 0x00, 0x00, 
	0x61, 0x62, 0x63, 0x64, 
	0x65, 0x66, 0x67
};

/// The first byte says that the gamedata has two files,
/// but there is only one. `gamedata_create` should return GD_FILE_CORRUPTED
static uint8_t buf_bad[] = {
	0x02, 0x00, 0x00, 0x00, 
	0x03, 0x00, 0x00, 0x00, 
	0x04, 0x00, 0x00, 0x00, 
	0x61, 0x62, 0x63, 0x64, 
	0x65, 0x66, 0x67
};

static char *
test_corruption_detection()
{
	int buf_size = sizeof(buf_bad) / sizeof(uint8_t);
	struct gamedata data;
	mu_assert(
	    "ERROR, gamedata_create(...) != GD_FILE_CORRUPTED\n",
	    gamedata_create(&data, buf_bad, buf_size) == GD_FILE_CORRUPTED
	);
	return 0;
}

static char *
test_ok()
{
	int buf_size = sizeof(buf_good) / sizeof(uint8_t);
	struct gamedata data;
	mu_assert(
	    "ERROR, gamedata_create(...) != GD_OK\n",
	    gamedata_create(&data, buf_good, buf_size) == GD_OK
	);
	return 0;
}

static char *
test_request_valid()
{
	int buf_size = sizeof(buf_good) / sizeof(uint8_t);
	struct gamedata data;
	gamedata_create(&data, buf_good, buf_size);

	struct file *f = gamedata_request(&data, "abcd");

	mu_assert("ERROR, gamedata_request(...) == NULL\n", f != NULL);
	mu_assert(
	    "ERROR, the content is different than expected!",
	    memcmp((const char *)f->content, "efg", f->size) == 0
	);
	return 0;
}

static char *
test_request_invalid()
{
	int buf_size = sizeof(buf_good) / sizeof(uint8_t);
	struct gamedata data;
	gamedata_create(&data, buf_good, buf_size);

	mu_assert(
	    "ERROR, gamedata_request(...) == NULL\n",
	    gamedata_request(&data, "invalid") == NULL
	);
	return 0;
}

static char *
all_tests()
{
	mu_run_test(test_corruption_detection);
	mu_run_test(test_ok);
	mu_run_test(test_request_valid);
	mu_run_test(test_request_invalid);
	return 0;
}

int
main(int argc, char **argv)
{
	char *result = all_tests();
	
	if (result != 0)
		fprintf(stderr, "%s\n", result);
	else
		fprintf(stderr, "GAMEDATA: ALL TESTS PASSED\n");

	fprintf(stderr, "Tests run: %d\n", tests_run);
	return result != 0;
}
