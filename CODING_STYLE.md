# Coding Style

Code style of the project is based on [Suckless's Coding Style](https://suckless.org/coding_style/). 

Code Style of the project may not agree with what is established here. If this is the case, then either the code is old and no one has had time to adapt it to the current style, or for some other reason. However, the current code should use this style by default and should follow it (if it doesn't, just report it!).

## C

- Use C99 without any extensions (like GNU C extensions), so any C extension is strictly prohibited!
- Use snake case
- Use tabs for **indentation** of size 4
- Use spaces for **alignment**
- Keep lines to a reasonable length (max 96 characters)

The following functions are prohibited and alternatives are given in parentheses (unless we don't have one):
- `sprintf` (`snprintf`)
- `atoi` (`strtoi` from `util.h`)
- `gets`
- `strcpy` (`memcpy`, `dstrcpy`)
  - `memcpy` takes an additional argument with the boundary

## Formatting

For formatting see [README.md](README.md)

## Comments

- Use `//` for comments, even in multiline comments
- Use `///` for documentation
- Use `/* */` if you want to comment out code (but such cases should be rare)
- If a comment explains ***why something is done the way it is***, add `NOTE(your nick):` above
  - If a comment explains ***how it's done***, don't do that
- Document code in headers if possible. It's useful for doxygen and (sometimes) for autocomplete

Examples:
```c
/// @brief <What this function does>
/// @return <What the function returns>
int foo() {
    // Normal comment

    // multiline
    // comment
    return 10;
}

/*
int useless() {
    return 5;
}
*/

int bar(int dog) {
    // NOTE(anon):
    // -1 crashes our application, so we need to ignore it
    if (dog == -1)
        return -1;

    return 0;
}
```

## Headers

Sort headers into three sections separated by two line breaks:

1. Related to the file you are creating
2. Headers from the C standard library, the `thirdparty` folder, and any libraries linked with the project
3. Headers from the project

Example:
```c
// foo.c
// 1
#include "foo.h"

// 2
#include <stdio.h>
#include <stdlib.h>
#include "stb/stb_ds.h"

// 3
#include "backends/render/render.h"
#include "common/graphics/surface.h"
```

- Header paths should be sorted in alphabetical order (clang-format should take care of that)
- Header path surrounded in string quotes `""` should be under paths surrounded in angle brackets `<>`
- For cases 1. and 3., surround the path in string quotes `""`. For case 2., surround the path in angle brackets `<>`

## Blocks

- `{` on the same line preceded by single space (except for functions)
- `}` on own line unless continuing statement 

Examples:
```c
while (true) {
    if (foo) {
        bar();
    }
}
```

```c
if (foo) {

} else {

}
```

## Functions

- Use snake_case for function names
- Return type and modifiers on own line
- Function name and argument list on next line
- Opening `{` on own line
- Functions not used outside translation unit should be declared and defined static.
- If a function can fail, and does not return any other type, use `int` as return type and negative numbers for errors
- If a function checks for the truth of some condition, for example: `is_valid_path()`, use `bool` as return type
- If a function doesn't take any arguments, remember to put `void` inside the argument list
- If a function argument is unused, add a `(void)arg` to the function body

Examples:
```c
static void
update(int a, void *b, char c)
{
    (void)a;
    (void)b;
    (void)c;

    foo();
}

static void
no_args(void)
{
    
}
```

## Structures

- Use snake case for struct names
- Opening `{` on the same line
- **Do not** typedef structs! Prefer `struct foo foo` declaration
- If you want to hide the struct definition, declare it in a header

Examples:
```c
struct foo {
    int bar;
};
```

```c
// foo.h
struct foo;

void foo_something();

// foo.c
struct foo {
    int bar;
}

void
foo_something()
{
    // ...
}
```

## Asserts

- Use asserts in functions for parameters that should not be NULL
- If you are comparing whether a pointer is NULL, prefer `assert(ptr != NULL)` over `assert(ptr)`

Example:
```c
void
foo(void *non_null)
{
    assert(non_null != NULL);
}
```

In this way, we can more easily catch misused functions in debug mode.

## Memory allocation

- **Do not** use `malloc`, `calloc` or `realloc`. Use our own alternatives: `emalloc`, `ecalloc` and `erealloc`.

Example:
```c
#include <stdio.h>
#include "common/util.h"

void 
foo()
{
    int *bar = emalloc(sizeof(int));
}
```
